<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sandbox
 */

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
 	<link href="https://fonts.googleapis.com/css?family=Raleway:400,700,700i" rel="stylesheet">
	 <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/barba.js/1.0.0/barba.min.js" type="text/javascript"></script>

</script>
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'sandbox-dev' ); ?></a>


	<div class="fixed-nava">
		<?php //sandbox_dev_display_widemenu(); ?>
		<?php
	$theme_options = get_option('tattoo_theme_settings');
  $select_field = $theme_options['select_menu_layout'];
	if($select_field == "center"){
		 sandbox_dev_display_centeredmenu();
	} else {
		 sandbox_dev_display_widemenu();

	}
	?>
	<?php
	    global $post;
	    $post_slug=$post->post_name;
	?>
	</div>
	<div id="barba-wrapper" data-namespace="<?php echo $post_slug; ?>">
  <div class="barba-container">
	<div id="content" class="site-content">

		<?php
			//sandbox_dev_enqueue_files();
		?>
