<?php
/**
 * Template Name: Home
 *
 * Template Post Type: page, scaffolding, sandbox_dev_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sandbox
 */

get_header(); ?>
<div class="hero-image">
	<div class="cta-sihatatto">
		<p><span>Barcelona</span>
			work<span>ink</span>class
		</p>
		<p class="another">Another</p>
		<div class="btn-cta">
				<p>Get Tattooed!!!</p>
		</div>

	</div>
</div>
	<div class="primary content-area">
		<main id="main" class="site-main">
			<h1>Home</h1>
			// Nuestro botón
<button id=“my-button””>Click me</button>

// Un párrafo donde mostrar el mensaje de respuesta
<p id="txtMessage">Nothing yet</p>
			<?php //do_action( 'sandbox_dev_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
