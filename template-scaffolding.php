<?php
/**
 * Template Name: Scaffolding
 *
 * Template Post Type: page, scaffolding, sandbox_dev_scaffolding
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sandbox
 */

get_header(); ?>

	<div class="primary content-area">
		<main id="main" class="site-main">
			<?php do_action( 'sandbox_dev_scaffolding_content' ); ?>
		</main><!-- #main -->
	</div><!-- .primary -->

<?php get_footer(); ?>
