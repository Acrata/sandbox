"use strict";

/**
 * Center Menu logo
 *
 * @author Hector Ovalles
 */
Barba.Pjax.start();
window.CenteredMenu = {};
(function (window, $, app) {
  var averageMenuItems = $("#centered-menu > li").size() / 2 - 1;
  $(".site-header-centered .site-branding").insertAfter("#centered-menu li:nth(" + averageMenuItems + ")");
  var lastElementClicked;
  Barba.Dispatcher.on('linkClicked', function (el) {
    lastElementClicked = el;
    console.log(el);
  });
  Barba.Dispatcher.on('newPageReady', function (currentStatus, oldStatus, container) {
    console.log("new page ready");
  });
  var FadeTransition = Barba.BaseTransition.extend({
    start: function start() {
      /**
       * This function is automatically called as soon the Transition starts
       * this.newContainerLoading is a Promise for the loading of the new container
       * (Barba.js also comes with an handy Promise polyfill!)
       */

      // As soon the loading is finished and the old page is faded out, let's fade the new page
      Promise.all([this.newContainerLoading, this.fadeOut()]).then(this.fadeIn.bind(this));
      //  console.log($(".hero-image"));
    },

    fadeOut: function fadeOut() {
      /**
       * this.oldContainer is the HTMLElement of the old Container
       */
      return $(this.oldContainer).addClass('is-changing-page').delay(1200).promise();
    },

    fadeIn: function fadeIn() {
      /**
       * this.newContainer is the HTMLElement of the new Container
       * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
       * Please note, newContainer is available just after newContainerLoading is resolved!
       */

      var _this = this;
      var $el = $(this.newContainer);
      var heroImage = $el.context.childNodes[1].childNodes[1];
      console.log($el.context.childNodes[1].childNodes[1]);
      // console.log(Barba.Pjax.Dom.dataNamespace);
      var children = $el.context.childNodes[1];
      //children.forEach(function(item){
      //console.log(item);
      // });
      //console.log($("#page"));
      // $("#page").css("transform","translate(50%)");
      //$(this.oldContainer).hide();
      console.log(TweenLite);

      console.log(TweenLite.from(heroImage, 1.3, {
        top: "123px"
      }));
      $el.css({
        visibility: 'visible',
        opacity: 0
      });

      $el.animate({ opacity: 1 }, 400, function () {
        /**
         * Do not forget to call .done() as soon your transition is finished!
         * .done() will automatically remove from the DOM the old Container
         */

        _this.done();
      });
    }
  });

  /**
   * Next step, you have to tell Barba to use the new Transition
   */

  Barba.Pjax.getTransition = function () {
    /**
     * Here you can use your own logic!
     * For example you can use different Transition based on the current page or link...
     */

    return FadeTransition;
  };
  var Homepage = Barba.BaseView.extend({
    namespace: 'home',
    onEnter: function onEnter() {
      // The new Container is ready and attached to the DOM.
      console.log("enter home");
    },
    onEnterCompleted: function onEnterCompleted() {
      // The Transition has just finished.
    },
    onLeave: function onLeave() {
      // A new Transition toward a new page has just started.
    },
    onLeaveCompleted: function onLeaveCompleted() {
      // The Container has just been removed from the DOM.
    }
  });

  // Don't forget to init the view!
  Homepage.init();
})(window, jQuery, window.CenteredMenu);
'use strict';

/**
 * Show/Hide the Search Form in the header.
 *
 * @author Corey Collins
 */
window.ShowHideSearchForm = {};
(function (window, $, app) {

	// Constructor
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things
	app.cache = function () {
		app.$c = {
			window: $(window),
			body: $('body'),
			headerSearchForm: $('.site-header-action .cta-button')
		};
	};

	// Combine all events
	app.bindEvents = function () {
		app.$c.headerSearchForm.on('keyup touchstart click', app.showHideSearchForm);
		app.$c.body.on('keyup touchstart click', app.hideSearchForm);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.headerSearchForm.length;
	};

	// Adds the toggle class for the search form.
	app.showHideSearchForm = function () {
		app.$c.body.toggleClass('search-form-visible');
	};

	// Hides the search form if we click outside of its container.
	app.hideSearchForm = function (event) {

		if (!$(event.target).parents('div').hasClass('site-header-action')) {
			app.$c.body.removeClass('search-form-visible');
		}
	};

	// Engage
	$(app.init);
})(window, jQuery, window.ShowHideSearchForm);
"use strict";

window.FixedMenu = {};
(function (window, $, app) {
  var headroom = new Headroom();
  $("#header").headroom({
    "offset": 20,
    "tolerance": 5,
    "classes": {
      "initial": "animated",
      "pinned": "slideInUp",
      "unpinned": "slideInDown"
    }
  });

  console.log(headroom);
})(window, jQuery, window.FixedMenu);
'use strict';

/**
 * File hero-carousel.js
 *
 * Create a carousel if we have more than one hero slide.
 */
window.wdsHeroCarousel = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			heroCarousel: $('.carousel')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.doSlick);
		app.$c.window.on('load', app.doFirstAnimation);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.heroCarousel.length;
	};

	// Animate the first slide on window load.
	app.doFirstAnimation = function () {

		// Get the first slide content area and animation attribute.
		var firstSlide = app.$c.heroCarousel.find('[data-slick-index=0]'),
		    firstSlideContent = firstSlide.find('.hero-content'),
		    firstAnimation = firstSlideContent.attr('data-animation');

		// Add the animation class to the first slide.
		firstSlideContent.addClass(firstAnimation);
	};

	// Animate the slide content.
	app.doAnimation = function () {
		var slides = $('.slide'),
		    activeSlide = $('.slick-current'),
		    activeContent = activeSlide.find('.hero-content'),


		// This is a string like so: 'animated someCssClass'.
		animationClass = activeContent.attr('data-animation'),
		    splitAnimation = animationClass.split(' '),


		// This is the 'animated' class.
		animationTrigger = splitAnimation[0];

		// Go through each slide to see if we've already set animation classes.
		slides.each(function () {
			var slideContent = $(this).find('.hero-content');

			// If we've set animation classes on a slide, remove them.
			if (slideContent.hasClass('animated')) {

				// Get the last class, which is the animate.css class.
				var lastClass = slideContent.attr('class').split(' ').pop();

				// Remove both animation classes.
				slideContent.removeClass(lastClass).removeClass(animationTrigger);
			}
		});

		// Add animation classes after slide is in view.
		activeContent.addClass(animationClass);
	};

	// Allow background videos to autoplay.
	app.playBackgroundVideos = function () {

		// Get all the videos in our slides object.
		$('video').each(function () {

			// Let them autoplay. TODO: Possibly change this later to only play the visible slide video.
			this.play();
		});
	};

	// Kick off Slick.
	app.doSlick = function () {
		app.$c.heroCarousel.on('init', app.playBackgroundVideos);

		app.$c.heroCarousel.slick({
			autoplay: true,
			autoplaySpeed: 5000,
			arrows: false,
			dots: false,
			focusOnSelect: true,
			waitForAnimate: true
		});

		app.$c.heroCarousel.on('afterChange', app.doAnimation);
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsHeroCarousel);
'use strict';

/**
 * File js-enabled.js
 *
 * If Javascript is enabled, replace the <body> class "no-js".
 */
document.body.className = document.body.className.replace('no-js', 'js');
"use strict";

var somevar = "somevar";
console.log(somevar);
window.MasonryGallery = {};
(function (window, $, app, Masonry) {
  // Constructor.
  app.init = function () {
    app.cacheSome();
  };
  app.cacheSome = function () {
    console.log("cached");
  };
  // var elem = document.querySelector('.gallery-artist-siha');
  // var msnry = new Masonry( elem, {
  //   // options
  //   itemSelector: '.gallery-item-sandbox',
  //   columnWidth: 200,
  //   percentPosition: true
  // });
  // console.log(elem);	// Engage!
  $(app.init);
})(window, jQuery, window.MasonryGallery, Masonry);
'use strict';

/**
 * File: mobile-menu.js
 *
 * Create an accordion style dropdown.
 */
window.wdsMobileMenu = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			body: $('body'),
			window: $(window),
			subMenuContainer: $('.mobile-menu .sub-menu, .utility-navigation .sub-menu'),
			subSubMenuContainer: $('.mobile-menu .sub-menu .sub-menu'),
			subMenuParentItem: $('.mobile-menu li.menu-item-has-children, .utility-navigation li.menu-item-has-children'),
			offCanvasContainer: $('.off-canvas-container')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.subMenuParentItem.on('click', app.toggleSubmenu);
		app.$c.subMenuParentItem.on('transitionend', app.resetSubMenu);
		app.$c.offCanvasContainer.on('transitionend', app.forceCloseSubmenus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Reset the submenus after it's done closing.
	app.resetSubMenu = function () {

		// When the list item is done transitioning in height,
		// remove the classes from the submenu so it is ready to toggle again.
		if ($(this).is('li.menu-item-has-children') && !$(this).hasClass('is-visible')) {
			$(this).find('ul.sub-menu').removeClass('slideOutLeft is-visible');
		}
	};

	// Slide out the submenu items.
	app.slideOutSubMenus = function (el) {

		// If this item's parent is visible and this is not, bail.
		if (el.parent().hasClass('is-visible') && !el.hasClass('is-visible')) {
			return;
		}

		// If this item's parent is visible and this item is visible, hide its submenu then bail.
		if (el.parent().hasClass('is-visible') && el.hasClass('is-visible')) {
			el.removeClass('is-visible').find('.sub-menu').removeClass('slideInLeft').addClass('slideOutLeft');
			return;
		}

		app.$c.subMenuContainer.each(function () {

			// Only try to close submenus that are actually open.
			if ($(this).hasClass('slideInLeft')) {

				// Close the parent list item, and set the corresponding button aria to false.
				$(this).parent().removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);

				// Slide out the submenu.
				$(this).removeClass('slideInLeft').addClass('slideOutLeft');
			}
		});
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.prepend('<button type="button" aria-expanded="false" class="parent-indicator" aria-label="Open submenu"><span class="down-arrow"></span></button>');
	};

	// Deal with the submenu.
	app.toggleSubmenu = function (e) {

		var el = $(this),
		    // The menu element which was clicked on.
		subMenu = el.children('ul.sub-menu'),
		    // The nearest submenu.
		$target = $(e.target); // the element that's actually being clicked (child of the li that triggered the click event).

		// Figure out if we're clicking the button or its arrow child,
		// if so, we can just open or close the menu and bail.
		if ($target.hasClass('down-arrow') || $target.hasClass('parent-indicator')) {

			// First, collapse any already opened submenus.
			app.slideOutSubMenus(el);

			if (!subMenu.hasClass('is-visible')) {

				// Open the submenu.
				app.openSubmenu(el, subMenu);
			}

			return false;
		}
	};

	// Open a submenu.
	app.openSubmenu = function (parent, subMenu) {

		// Expand the list menu item, and set the corresponding button aria to true.
		parent.addClass('is-visible').find('.parent-indicator').attr('aria-expanded', true);

		// Slide the menu in.
		subMenu.addClass('is-visible animated slideInLeft');
	};

	// Force close all the submenus when the main menu container is closed.
	app.forceCloseSubmenus = function () {

		// The transitionend event triggers on open and on close, need to make sure we only do this on close.
		if (!$(this).hasClass('is-visible')) {
			app.$c.subMenuParentItem.removeClass('is-visible').find('.parent-indicator').attr('aria-expanded', false);
			app.$c.subMenuContainer.removeClass('is-visible slideInLeft');
			app.$c.body.css('overflow', 'visible');
			app.$c.body.unbind('touchstart');
		}

		if ($(this).hasClass('is-visible')) {
			app.$c.body.css('overflow', 'hidden');
			app.$c.body.bind('touchstart', function (e) {
				if (!$(e.target).parents('.contact-modal')[0]) {
					e.preventDefault();
				}
			});
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsMobileMenu);
'use strict';

/**
 * File modal.js
 *
 * Deal with multiple modals and their media.
 */
window.wdsModal = {};
(function (window, $, app) {

	var $modalToggle = void 0,
	    $focusableChildren = void 0,
	    $player = void 0,
	    $tag = document.createElement('script'),
	    $firstScriptTag = document.getElementsByTagName('script')[0],
	    YT = void 0;

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			$firstScriptTag.parentNode.insertBefore($tag, $firstScriptTag);
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			'body': $('body')
		};
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return $('.modal-trigger').length;
	};

	// Combine all events.
	app.bindEvents = function () {

		// Trigger a modal to open.
		app.$c.body.on('click touchstart', '.modal-trigger', app.openModal);

		// Trigger the close button to close the modal.
		app.$c.body.on('click touchstart', '.close', app.closeModal);

		// Allow the user to close the modal by hitting the esc key.
		app.$c.body.on('keydown', app.escKeyClose);

		// Allow the user to close the modal by clicking outside of the modal.
		app.$c.body.on('click touchstart', 'div.modal-open', app.closeModalByClick);

		// Listen to tabs, trap keyboard if we need to
		app.$c.body.on('keydown', app.trapKeyboardMaybe);
	};

	// Open the modal.
	app.openModal = function () {

		// Store the modal toggle element
		$modalToggle = $(this);

		// Figure out which modal we're opening and store the object.
		var $modal = $($(this).data('target'));

		// Display the modal.
		$modal.addClass('modal-open');

		// Add body class.
		app.$c.body.addClass('modal-open');

		// Find the focusable children of the modal.
		// This list may be incomplete, really wish jQuery had the :focusable pseudo like jQuery UI does.
		// For more about :input see: https://api.jquery.com/input-selector/
		$focusableChildren = $modal.find('a, :input, [tabindex]');

		// Ideally, there is always one (the close button), but you never know.
		if (0 < $focusableChildren.length) {

			// Shift focus to the first focusable element.
			$focusableChildren[0].focus();
		}
	};

	// Close the modal.
	app.closeModal = function () {

		// Figure the opened modal we're closing and store the object.
		var $modal = $($('div.modal-open .close').data('target')),


		// Find the iframe in the $modal object.
		$iframe = $modal.find('iframe');

		// Only do this if there are any iframes.
		if ($iframe.length) {

			// Get the iframe src URL.
			var url = $iframe.attr('src');

			// Removing/Readding the URL will effectively break the YouTube API.
			// So let's not do that when the iframe URL contains the enablejsapi parameter.
			if (!url.includes('enablejsapi=1')) {

				// Remove the source URL, then add it back, so the video can be played again later.
				$iframe.attr('src', '').attr('src', url);
			} else {

				// Use the YouTube API to stop the video.
				$player.stopVideo();
			}
		}

		// Finally, hide the modal.
		$modal.removeClass('modal-open');

		// Remove the body class.
		app.$c.body.removeClass('modal-open');

		// Revert focus back to toggle element
		$modalToggle.focus();
	};

	// Close if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeModal();
		}
	};

	// Close if the user clicks outside of the modal
	app.closeModalByClick = function (event) {

		// If the parent container is NOT the modal dialog container, close the modal
		if (!$(event.target).parents('div').hasClass('modal-dialog')) {
			app.closeModal();
		}
	};

	// Trap the keyboard into a modal when one is active.
	app.trapKeyboardMaybe = function (event) {

		// We only need to do stuff when the modal is open and tab is pressed.
		if (9 === event.which && 0 < $('.modal-open').length) {
			var $focused = $(':focus'),
			    focusIndex = $focusableChildren.index($focused);

			if (0 === focusIndex && event.shiftKey) {

				// If this is the first focusable element, and shift is held when pressing tab, go back to last focusable element.
				$focusableChildren[$focusableChildren.length - 1].focus();
				event.preventDefault();
			} else if (!event.shiftKey && focusIndex === $focusableChildren.length - 1) {

				// If this is the last focusable element, and shift is not held, go back to the first focusable element.
				$focusableChildren[0].focus();
				event.preventDefault();
			}
		}
	};

	// Hook into YouTube <iframe>.
	app.onYouTubeIframeAPIReady = function () {
		var $modal = $('div.modal'),
		    $iframeid = $modal.find('iframe').attr('id');

		$player = new YT.Player($iframeid, {
			events: {
				'onReady': app.onPlayerReady,
				'onStateChange': app.onPlayerStateChange
			}
		});
	};

	// Do something on player ready.
	app.onPlayerReady = function () {};

	// Do something on player state change.
	app.onPlayerStateChange = function () {

		// Set focus to the first focusable element inside of the modal the player is in.
		$(event.target.a).parents('.modal').find('a, :input, [tabindex]').first().focus();
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsModal);
'use strict';

/**
 * File: navigation-primary.js
 *
 * Helpers for the primary navigation.
 */
window.wdsPrimaryNavigation = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			window: $(window),
			subMenuContainer: $('.main-navigation .sub-menu'),
			subMenuParentItem: $('.main-navigation li.menu-item-has-children')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.on('load', app.addDownArrow);
		app.$c.subMenuParentItem.find('a').on('focusin focusout', app.toggleFocus);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.subMenuContainer.length;
	};

	// Add the down arrow to submenu parents.
	app.addDownArrow = function () {
		app.$c.subMenuParentItem.find('> a').append('<span class="caret-down" aria-hidden="true"></span>');
	};

	// Toggle the focus class on the link parent.
	app.toggleFocus = function () {
		$(this).parents('li.menu-item-has-children').toggleClass('focus');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsPrimaryNavigation);
'use strict';

/**
 * File: off-canvas.js
 *
 * Help deal with the off-canvas mobile menu.
 */
window.wdsoffCanvas = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();

		if (app.meetsRequirements()) {
			app.bindEvents();
		}
	};

	// Cache all the things.
	app.cache = function () {
		app.$c = {
			body: $('body'),
			offCanvasClose: $('.off-canvas-close'),
			offCanvasContainer: $('.off-canvas-container'),
			offCanvasOpen: $('.off-canvas-open'),
			offCanvasScreen: $('.off-canvas-screen')
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.body.on('keydown', app.escKeyClose);
		app.$c.offCanvasClose.on('click', app.closeoffCanvas);
		app.$c.offCanvasOpen.on('click', app.toggleoffCanvas);
		app.$c.offCanvasScreen.on('click', app.closeoffCanvas);
	};

	// Do we meet the requirements?
	app.meetsRequirements = function () {
		return app.$c.offCanvasContainer.length;
	};

	// To show or not to show?
	app.toggleoffCanvas = function () {

		if ('true' === $(this).attr('aria-expanded')) {
			app.closeoffCanvas();
		} else {
			app.openoffCanvas();
		}
	};

	// Show that drawer!
	app.openoffCanvas = function () {
		app.$c.offCanvasContainer.addClass('is-visible');
		app.$c.offCanvasOpen.addClass('is-visible');
		app.$c.offCanvasScreen.addClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', true);
		app.$c.offCanvasContainer.attr('aria-hidden', false);

		app.$c.offCanvasContainer.find('button').first().focus();
	};

	// Close that drawer!
	app.closeoffCanvas = function () {
		app.$c.offCanvasContainer.removeClass('is-visible');
		app.$c.offCanvasOpen.removeClass('is-visible');
		app.$c.offCanvasScreen.removeClass('is-visible');

		app.$c.offCanvasOpen.attr('aria-expanded', false);
		app.$c.offCanvasContainer.attr('aria-hidden', true);

		app.$c.offCanvasOpen.focus();
	};

	// Close drawer if "esc" key is pressed.
	app.escKeyClose = function (event) {
		if (27 === event.keyCode) {
			app.closeoffCanvas();
		}
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsoffCanvas);
'use strict';

/**
 * File skip-link-focus-fix.js.
 *
 * Helps with accessibility for keyboard only users.
 *
 * Learn more: https://git.io/vWdr2
 */
(function () {
	var isWebkit = -1 < navigator.userAgent.toLowerCase().indexOf('webkit'),
	    isOpera = -1 < navigator.userAgent.toLowerCase().indexOf('opera'),
	    isIe = -1 < navigator.userAgent.toLowerCase().indexOf('msie');

	if ((isWebkit || isOpera || isIe) && document.getElementById && window.addEventListener) {
		window.addEventListener('hashchange', function () {
			var id = location.hash.substring(1),
			    element;

			if (!/^[A-z0-9_-]+$/.test(id)) {
				return;
			}

			element = document.getElementById(id);

			if (element) {
				if (!/^(?:a|select|input|button|textarea)$/i.test(element.tagName)) {
					element.tabIndex = -1;
				}

				element.focus();
			}
		}, false);
	}
})();
'use strict';

/**
 * File window-ready.js
 *
 * Add a "ready" class to <body> when window is ready.
 */
window.wdsWindowReady = {};
(function (window, $, app) {

	// Constructor.
	app.init = function () {
		app.cache();
		app.bindEvents();
	};

	// Cache document elements.
	app.cache = function () {
		app.$c = {
			'window': $(window),
			'body': $(document.body)
		};
	};

	// Combine all events.
	app.bindEvents = function () {
		app.$c.window.load(app.addBodyClass);
	};

	// Add a class to <body>.
	app.addBodyClass = function () {
		app.$c.body.addClass('ready');
	};

	// Engage!
	$(app.init);
})(window, jQuery, window.wdsWindowReady);
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNlbnRlcmVkLmpzIiwiaGVhZGVyLWJ1dHRvbi5qcyIsImhlYWRyb29tLmpzIiwiaGVyby1jYXJvdXNlbC5qcyIsImpzLWVuYWJsZWQuanMiLCJtYXNvbnJ5LWdhbGxlcnkuanMiLCJtb2JpbGUtbWVudS5qcyIsIm1vZGFsLmpzIiwibmF2aWdhdGlvbi1wcmltYXJ5LmpzIiwib2ZmLWNhbnZhcy5qcyIsInNraXAtbGluay1mb2N1cy1maXguanMiLCJ3aW5kb3ctcmVhZHkuanMiXSwibmFtZXMiOlsiQmFyYmEiLCJQamF4Iiwic3RhcnQiLCJ3aW5kb3ciLCJDZW50ZXJlZE1lbnUiLCIkIiwiYXBwIiwiYXZlcmFnZU1lbnVJdGVtcyIsInNpemUiLCJpbnNlcnRBZnRlciIsImxhc3RFbGVtZW50Q2xpY2tlZCIsIkRpc3BhdGNoZXIiLCJvbiIsImVsIiwiY29uc29sZSIsImxvZyIsImN1cnJlbnRTdGF0dXMiLCJvbGRTdGF0dXMiLCJjb250YWluZXIiLCJGYWRlVHJhbnNpdGlvbiIsIkJhc2VUcmFuc2l0aW9uIiwiZXh0ZW5kIiwiUHJvbWlzZSIsImFsbCIsIm5ld0NvbnRhaW5lckxvYWRpbmciLCJmYWRlT3V0IiwidGhlbiIsImZhZGVJbiIsImJpbmQiLCJvbGRDb250YWluZXIiLCJhZGRDbGFzcyIsImRlbGF5IiwicHJvbWlzZSIsIl90aGlzIiwiJGVsIiwibmV3Q29udGFpbmVyIiwiaGVyb0ltYWdlIiwiY29udGV4dCIsImNoaWxkTm9kZXMiLCJjaGlsZHJlbiIsIlR3ZWVuTGl0ZSIsImZyb20iLCJ0b3AiLCJjc3MiLCJ2aXNpYmlsaXR5Iiwib3BhY2l0eSIsImFuaW1hdGUiLCJkb25lIiwiZ2V0VHJhbnNpdGlvbiIsIkhvbWVwYWdlIiwiQmFzZVZpZXciLCJuYW1lc3BhY2UiLCJvbkVudGVyIiwib25FbnRlckNvbXBsZXRlZCIsIm9uTGVhdmUiLCJvbkxlYXZlQ29tcGxldGVkIiwiaW5pdCIsImpRdWVyeSIsIlNob3dIaWRlU2VhcmNoRm9ybSIsImNhY2hlIiwibWVldHNSZXF1aXJlbWVudHMiLCJiaW5kRXZlbnRzIiwiJGMiLCJib2R5IiwiaGVhZGVyU2VhcmNoRm9ybSIsInNob3dIaWRlU2VhcmNoRm9ybSIsImhpZGVTZWFyY2hGb3JtIiwibGVuZ3RoIiwidG9nZ2xlQ2xhc3MiLCJldmVudCIsInRhcmdldCIsInBhcmVudHMiLCJoYXNDbGFzcyIsInJlbW92ZUNsYXNzIiwiRml4ZWRNZW51IiwiaGVhZHJvb20iLCJIZWFkcm9vbSIsIndkc0hlcm9DYXJvdXNlbCIsImhlcm9DYXJvdXNlbCIsImRvU2xpY2siLCJkb0ZpcnN0QW5pbWF0aW9uIiwiZmlyc3RTbGlkZSIsImZpbmQiLCJmaXJzdFNsaWRlQ29udGVudCIsImZpcnN0QW5pbWF0aW9uIiwiYXR0ciIsImRvQW5pbWF0aW9uIiwic2xpZGVzIiwiYWN0aXZlU2xpZGUiLCJhY3RpdmVDb250ZW50IiwiYW5pbWF0aW9uQ2xhc3MiLCJzcGxpdEFuaW1hdGlvbiIsInNwbGl0IiwiYW5pbWF0aW9uVHJpZ2dlciIsImVhY2giLCJzbGlkZUNvbnRlbnQiLCJsYXN0Q2xhc3MiLCJwb3AiLCJwbGF5QmFja2dyb3VuZFZpZGVvcyIsInBsYXkiLCJzbGljayIsImF1dG9wbGF5IiwiYXV0b3BsYXlTcGVlZCIsImFycm93cyIsImRvdHMiLCJmb2N1c09uU2VsZWN0Iiwid2FpdEZvckFuaW1hdGUiLCJkb2N1bWVudCIsImNsYXNzTmFtZSIsInJlcGxhY2UiLCJzb21ldmFyIiwiTWFzb25yeUdhbGxlcnkiLCJNYXNvbnJ5IiwiY2FjaGVTb21lIiwid2RzTW9iaWxlTWVudSIsInN1Yk1lbnVDb250YWluZXIiLCJzdWJTdWJNZW51Q29udGFpbmVyIiwic3ViTWVudVBhcmVudEl0ZW0iLCJvZmZDYW52YXNDb250YWluZXIiLCJhZGREb3duQXJyb3ciLCJ0b2dnbGVTdWJtZW51IiwicmVzZXRTdWJNZW51IiwiZm9yY2VDbG9zZVN1Ym1lbnVzIiwiaXMiLCJzbGlkZU91dFN1Yk1lbnVzIiwicGFyZW50IiwicHJlcGVuZCIsImUiLCJzdWJNZW51IiwiJHRhcmdldCIsIm9wZW5TdWJtZW51IiwidW5iaW5kIiwicHJldmVudERlZmF1bHQiLCJ3ZHNNb2RhbCIsIiRtb2RhbFRvZ2dsZSIsIiRmb2N1c2FibGVDaGlsZHJlbiIsIiRwbGF5ZXIiLCIkdGFnIiwiY3JlYXRlRWxlbWVudCIsIiRmaXJzdFNjcmlwdFRhZyIsImdldEVsZW1lbnRzQnlUYWdOYW1lIiwiWVQiLCJwYXJlbnROb2RlIiwiaW5zZXJ0QmVmb3JlIiwib3Blbk1vZGFsIiwiY2xvc2VNb2RhbCIsImVzY0tleUNsb3NlIiwiY2xvc2VNb2RhbEJ5Q2xpY2siLCJ0cmFwS2V5Ym9hcmRNYXliZSIsIiRtb2RhbCIsImRhdGEiLCJmb2N1cyIsIiRpZnJhbWUiLCJ1cmwiLCJpbmNsdWRlcyIsInN0b3BWaWRlbyIsImtleUNvZGUiLCJ3aGljaCIsIiRmb2N1c2VkIiwiZm9jdXNJbmRleCIsImluZGV4Iiwic2hpZnRLZXkiLCJvbllvdVR1YmVJZnJhbWVBUElSZWFkeSIsIiRpZnJhbWVpZCIsIlBsYXllciIsImV2ZW50cyIsIm9uUGxheWVyUmVhZHkiLCJvblBsYXllclN0YXRlQ2hhbmdlIiwiYSIsImZpcnN0Iiwid2RzUHJpbWFyeU5hdmlnYXRpb24iLCJ0b2dnbGVGb2N1cyIsImFwcGVuZCIsIndkc29mZkNhbnZhcyIsIm9mZkNhbnZhc0Nsb3NlIiwib2ZmQ2FudmFzT3BlbiIsIm9mZkNhbnZhc1NjcmVlbiIsImNsb3Nlb2ZmQ2FudmFzIiwidG9nZ2xlb2ZmQ2FudmFzIiwib3Blbm9mZkNhbnZhcyIsImlzV2Via2l0IiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwidG9Mb3dlckNhc2UiLCJpbmRleE9mIiwiaXNPcGVyYSIsImlzSWUiLCJnZXRFbGVtZW50QnlJZCIsImFkZEV2ZW50TGlzdGVuZXIiLCJpZCIsImxvY2F0aW9uIiwiaGFzaCIsInN1YnN0cmluZyIsImVsZW1lbnQiLCJ0ZXN0IiwidGFnTmFtZSIsInRhYkluZGV4Iiwid2RzV2luZG93UmVhZHkiLCJsb2FkIiwiYWRkQm9keUNsYXNzIl0sIm1hcHBpbmdzIjoiOztBQUFBOzs7OztBQUtBQSxNQUFNQyxJQUFOLENBQVdDLEtBQVg7QUFDQUMsT0FBT0MsWUFBUCxHQUFzQixFQUF0QjtBQUNFLFdBQVVELE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjtBQUMzQixNQUFJQyxtQkFBbUJGLEVBQUUscUJBQUYsRUFBeUJHLElBQXpCLEtBQWdDLENBQWhDLEdBQWtDLENBQXpEO0FBQ0FILElBQUUsc0NBQUYsRUFBMENJLFdBQTFDLENBQXNELDJCQUEwQkYsZ0JBQTFCLEdBQTRDLEdBQWxHO0FBQ0EsTUFBSUcsa0JBQUo7QUFDQVYsUUFBTVcsVUFBTixDQUFpQkMsRUFBakIsQ0FBb0IsYUFBcEIsRUFBbUMsVUFBU0MsRUFBVCxFQUFhO0FBQ2hESCx5QkFBcUJHLEVBQXJCO0FBQ0FDLFlBQVFDLEdBQVIsQ0FBWUYsRUFBWjtBQUNELEdBSEM7QUFJRmIsUUFBTVcsVUFBTixDQUFpQkMsRUFBakIsQ0FBb0IsY0FBcEIsRUFBb0MsVUFBU0ksYUFBVCxFQUF3QkMsU0FBeEIsRUFBbUNDLFNBQW5DLEVBQThDO0FBQ2xGSixZQUFRQyxHQUFSLENBQVksZ0JBQVo7QUFDQyxHQUZEO0FBR0UsTUFBSUksaUJBQWlCbkIsTUFBTW9CLGNBQU4sQ0FBcUJDLE1BQXJCLENBQTRCO0FBQy9DbkIsV0FBTyxpQkFBVztBQUNoQjs7Ozs7O0FBTUE7QUFDQW9CLGNBQ0dDLEdBREgsQ0FDTyxDQUFDLEtBQUtDLG1CQUFOLEVBQTJCLEtBQUtDLE9BQUwsRUFBM0IsQ0FEUCxFQUVHQyxJQUZILENBRVEsS0FBS0MsTUFBTCxDQUFZQyxJQUFaLENBQWlCLElBQWpCLENBRlI7QUFHSTtBQUNMLEtBYjhDOztBQWUvQ0gsYUFBUyxtQkFBVztBQUNsQjs7O0FBR0EsYUFBT3BCLEVBQUUsS0FBS3dCLFlBQVAsRUFBcUJDLFFBQXJCLENBQThCLGtCQUE5QixFQUFrREMsS0FBbEQsQ0FBd0QsSUFBeEQsRUFBOERDLE9BQTlELEVBQVA7QUFDRCxLQXBCOEM7O0FBc0IvQ0wsWUFBUSxrQkFBVztBQUNqQjs7Ozs7O0FBTUEsVUFBSU0sUUFBUSxJQUFaO0FBQ0EsVUFBSUMsTUFBTTdCLEVBQUUsS0FBSzhCLFlBQVAsQ0FBVjtBQUNBLFVBQUlDLFlBQVlGLElBQUlHLE9BQUosQ0FBWUMsVUFBWixDQUF1QixDQUF2QixFQUEwQkEsVUFBMUIsQ0FBcUMsQ0FBckMsQ0FBaEI7QUFDQ3hCLGNBQVFDLEdBQVIsQ0FBWW1CLElBQUlHLE9BQUosQ0FBWUMsVUFBWixDQUF1QixDQUF2QixFQUEwQkEsVUFBMUIsQ0FBcUMsQ0FBckMsQ0FBWjtBQUNBO0FBQ0EsVUFBSUMsV0FBV0wsSUFBSUcsT0FBSixDQUFZQyxVQUFaLENBQXVCLENBQXZCLENBQWY7QUFDUDtBQUNJO0FBQ0o7QUFDTTtBQUNBO0FBQ0E7QUFDQXhCLGNBQVFDLEdBQVIsQ0FBWXlCLFNBQVo7O0FBRUYxQixjQUFRQyxHQUFSLENBQVl5QixVQUFVQyxJQUFWLENBQWVMLFNBQWYsRUFBMEIsR0FBMUIsRUFBK0I7QUFDdkNNLGFBQUs7QUFEa0MsT0FBL0IsQ0FBWjtBQUdFUixVQUFJUyxHQUFKLENBQVE7QUFDTkMsb0JBQWEsU0FEUDtBQUVOQyxpQkFBVTtBQUZKLE9BQVI7O0FBS0FYLFVBQUlZLE9BQUosQ0FBWSxFQUFFRCxTQUFTLENBQVgsRUFBWixFQUE2QixHQUE3QixFQUFrQyxZQUFXO0FBQzNDOzs7OztBQUtBWixjQUFNYyxJQUFOO0FBQ0QsT0FQRDtBQVFEO0FBM0Q4QyxHQUE1QixDQUFyQjs7QUE4REE7Ozs7QUFJQS9DLFFBQU1DLElBQU4sQ0FBVytDLGFBQVgsR0FBMkIsWUFBVztBQUNwQzs7Ozs7QUFLQSxXQUFPN0IsY0FBUDtBQUNELEdBUEQ7QUFRQSxNQUFJOEIsV0FBV2pELE1BQU1rRCxRQUFOLENBQWU3QixNQUFmLENBQXNCO0FBQ3JDOEIsZUFBVyxNQUQwQjtBQUVyQ0MsYUFBUyxtQkFBVztBQUNoQjtBQUNBdEMsY0FBUUMsR0FBUixDQUFZLFlBQVo7QUFDSCxLQUxvQztBQU1yQ3NDLHNCQUFrQiw0QkFBVztBQUN6QjtBQUNILEtBUm9DO0FBU3JDQyxhQUFTLG1CQUFXO0FBQ2hCO0FBQ0gsS0FYb0M7QUFZckNDLHNCQUFrQiw0QkFBVztBQUN6QjtBQUNIO0FBZG9DLEdBQXRCLENBQWY7O0FBaUJGO0FBQ0FOLFdBQVNPLElBQVQ7QUFFQyxDQXpHQyxFQXlHRXJELE1BekdGLEVBeUdVc0QsTUF6R1YsRUF5R2tCdEQsT0FBT0MsWUF6R3pCLENBQUY7OztBQ1BBOzs7OztBQUtBRCxPQUFPdUQsa0JBQVAsR0FBNEIsRUFBNUI7QUFDRSxXQUFVdkQsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTJCOztBQUU1QjtBQUNBQSxLQUFJa0QsSUFBSixHQUFXLFlBQVc7QUFDckJsRCxNQUFJcUQsS0FBSjs7QUFFQSxNQUFLckQsSUFBSXNELGlCQUFKLEVBQUwsRUFBK0I7QUFDOUJ0RCxPQUFJdUQsVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBdkQsS0FBSXFELEtBQUosR0FBWSxZQUFXO0FBQ3RCckQsTUFBSXdELEVBQUosR0FBUztBQUNSM0QsV0FBUUUsRUFBR0YsTUFBSCxDQURBO0FBRVI0RCxTQUFNMUQsRUFBRyxNQUFILENBRkU7QUFHUjJELHFCQUFrQjNELEVBQUcsaUNBQUg7QUFIVixHQUFUO0FBS0EsRUFORDs7QUFRQTtBQUNBQyxLQUFJdUQsVUFBSixHQUFpQixZQUFXO0FBQzNCdkQsTUFBSXdELEVBQUosQ0FBT0UsZ0JBQVAsQ0FBd0JwRCxFQUF4QixDQUE0Qix3QkFBNUIsRUFBc0ROLElBQUkyRCxrQkFBMUQ7QUFDQTNELE1BQUl3RCxFQUFKLENBQU9DLElBQVAsQ0FBWW5ELEVBQVosQ0FBZ0Isd0JBQWhCLEVBQTBDTixJQUFJNEQsY0FBOUM7QUFDQSxFQUhEOztBQUtBO0FBQ0E1RCxLQUFJc0QsaUJBQUosR0FBd0IsWUFBVztBQUNsQyxTQUFPdEQsSUFBSXdELEVBQUosQ0FBT0UsZ0JBQVAsQ0FBd0JHLE1BQS9CO0FBQ0EsRUFGRDs7QUFJQTtBQUNBN0QsS0FBSTJELGtCQUFKLEdBQXlCLFlBQVc7QUFDbkMzRCxNQUFJd0QsRUFBSixDQUFPQyxJQUFQLENBQVlLLFdBQVosQ0FBeUIscUJBQXpCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBOUQsS0FBSTRELGNBQUosR0FBcUIsVUFBVUcsS0FBVixFQUFrQjs7QUFFdEMsTUFBSyxDQUFFaEUsRUFBR2dFLE1BQU1DLE1BQVQsRUFBa0JDLE9BQWxCLENBQTJCLEtBQTNCLEVBQW1DQyxRQUFuQyxDQUE2QyxvQkFBN0MsQ0FBUCxFQUE2RTtBQUM1RWxFLE9BQUl3RCxFQUFKLENBQU9DLElBQVAsQ0FBWVUsV0FBWixDQUF5QixxQkFBekI7QUFDQTtBQUNELEVBTEQ7O0FBT0E7QUFDQXBFLEdBQUdDLElBQUlrRCxJQUFQO0FBRUEsQ0EvQ0MsRUErQ0VyRCxNQS9DRixFQStDVXNELE1BL0NWLEVBK0NrQnRELE9BQU91RCxrQkEvQ3pCLENBQUY7OztBQ05BdkQsT0FBT3VFLFNBQVAsR0FBbUIsRUFBbkI7QUFDRSxXQUFVdkUsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTBCO0FBQzVCLE1BQUlxRSxXQUFXLElBQUlDLFFBQUosRUFBZjtBQUNBdkUsSUFBRSxTQUFGLEVBQWFzRSxRQUFiLENBQXNCO0FBQ3BCLGNBQVUsRUFEVTtBQUVwQixpQkFBYSxDQUZPO0FBR3BCLGVBQVc7QUFDVCxpQkFBVyxVQURGO0FBRVQsZ0JBQVUsV0FGRDtBQUdULGtCQUFZO0FBSEg7QUFIUyxHQUF0Qjs7QUFVQTdELFVBQVFDLEdBQVIsQ0FBWTRELFFBQVo7QUFDQyxDQWJDLEVBYUV4RSxNQWJGLEVBYVVzRCxNQWJWLEVBYWtCdEQsT0FBT3VFLFNBYnpCLENBQUY7OztBQ0RBOzs7OztBQUtBdkUsT0FBTzBFLGVBQVAsR0FBeUIsRUFBekI7QUFDRSxXQUFVMUUsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTJCOztBQUU1QjtBQUNBQSxLQUFJa0QsSUFBSixHQUFXLFlBQVc7QUFDckJsRCxNQUFJcUQsS0FBSjs7QUFFQSxNQUFLckQsSUFBSXNELGlCQUFKLEVBQUwsRUFBK0I7QUFDOUJ0RCxPQUFJdUQsVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBdkQsS0FBSXFELEtBQUosR0FBWSxZQUFXO0FBQ3RCckQsTUFBSXdELEVBQUosR0FBUztBQUNSM0QsV0FBUUUsRUFBR0YsTUFBSCxDQURBO0FBRVIyRSxpQkFBY3pFLEVBQUcsV0FBSDtBQUZOLEdBQVQ7QUFJQSxFQUxEOztBQU9BO0FBQ0FDLEtBQUl1RCxVQUFKLEdBQWlCLFlBQVc7QUFDM0J2RCxNQUFJd0QsRUFBSixDQUFPM0QsTUFBUCxDQUFjUyxFQUFkLENBQWtCLE1BQWxCLEVBQTBCTixJQUFJeUUsT0FBOUI7QUFDQXpFLE1BQUl3RCxFQUFKLENBQU8zRCxNQUFQLENBQWNTLEVBQWQsQ0FBa0IsTUFBbEIsRUFBMEJOLElBQUkwRSxnQkFBOUI7QUFDQSxFQUhEOztBQUtBO0FBQ0ExRSxLQUFJc0QsaUJBQUosR0FBd0IsWUFBVztBQUNsQyxTQUFPdEQsSUFBSXdELEVBQUosQ0FBT2dCLFlBQVAsQ0FBb0JYLE1BQTNCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBN0QsS0FBSTBFLGdCQUFKLEdBQXVCLFlBQVc7O0FBRWpDO0FBQ0EsTUFBSUMsYUFBYTNFLElBQUl3RCxFQUFKLENBQU9nQixZQUFQLENBQW9CSSxJQUFwQixDQUEwQixzQkFBMUIsQ0FBakI7QUFBQSxNQUNDQyxvQkFBb0JGLFdBQVdDLElBQVgsQ0FBaUIsZUFBakIsQ0FEckI7QUFBQSxNQUVDRSxpQkFBaUJELGtCQUFrQkUsSUFBbEIsQ0FBd0IsZ0JBQXhCLENBRmxCOztBQUlBO0FBQ0FGLG9CQUFrQnJELFFBQWxCLENBQTRCc0QsY0FBNUI7QUFDQSxFQVREOztBQVdBO0FBQ0E5RSxLQUFJZ0YsV0FBSixHQUFrQixZQUFXO0FBQzVCLE1BQUlDLFNBQVNsRixFQUFHLFFBQUgsQ0FBYjtBQUFBLE1BQ0NtRixjQUFjbkYsRUFBRyxnQkFBSCxDQURmO0FBQUEsTUFFQ29GLGdCQUFnQkQsWUFBWU4sSUFBWixDQUFrQixlQUFsQixDQUZqQjs7O0FBSUM7QUFDQVEsbUJBQWlCRCxjQUFjSixJQUFkLENBQW9CLGdCQUFwQixDQUxsQjtBQUFBLE1BTUNNLGlCQUFpQkQsZUFBZUUsS0FBZixDQUFzQixHQUF0QixDQU5sQjs7O0FBUUM7QUFDQUMscUJBQW1CRixlQUFlLENBQWYsQ0FUcEI7O0FBV0E7QUFDQUosU0FBT08sSUFBUCxDQUFhLFlBQVc7QUFDdkIsT0FBSUMsZUFBZTFGLEVBQUcsSUFBSCxFQUFVNkUsSUFBVixDQUFnQixlQUFoQixDQUFuQjs7QUFFQTtBQUNBLE9BQUthLGFBQWF2QixRQUFiLENBQXVCLFVBQXZCLENBQUwsRUFBMkM7O0FBRTFDO0FBQ0EsUUFBSXdCLFlBQVlELGFBQ2RWLElBRGMsQ0FDUixPQURRLEVBRWRPLEtBRmMsQ0FFUCxHQUZPLEVBR2RLLEdBSGMsRUFBaEI7O0FBS0E7QUFDQUYsaUJBQWF0QixXQUFiLENBQTBCdUIsU0FBMUIsRUFBc0N2QixXQUF0QyxDQUFtRG9CLGdCQUFuRDtBQUNBO0FBQ0QsR0FmRDs7QUFpQkE7QUFDQUosZ0JBQWMzRCxRQUFkLENBQXdCNEQsY0FBeEI7QUFDQSxFQWhDRDs7QUFrQ0E7QUFDQXBGLEtBQUk0RixvQkFBSixHQUEyQixZQUFXOztBQUVyQztBQUNBN0YsSUFBRyxPQUFILEVBQWF5RixJQUFiLENBQW1CLFlBQVc7O0FBRTdCO0FBQ0EsUUFBS0ssSUFBTDtBQUNBLEdBSkQ7QUFLQSxFQVJEOztBQVVBO0FBQ0E3RixLQUFJeUUsT0FBSixHQUFjLFlBQVc7QUFDeEJ6RSxNQUFJd0QsRUFBSixDQUFPZ0IsWUFBUCxDQUFvQmxFLEVBQXBCLENBQXdCLE1BQXhCLEVBQWdDTixJQUFJNEYsb0JBQXBDOztBQUVBNUYsTUFBSXdELEVBQUosQ0FBT2dCLFlBQVAsQ0FBb0JzQixLQUFwQixDQUEyQjtBQUMxQkMsYUFBVSxJQURnQjtBQUUxQkMsa0JBQWUsSUFGVztBQUcxQkMsV0FBUSxLQUhrQjtBQUkxQkMsU0FBTSxLQUpvQjtBQUsxQkMsa0JBQWUsSUFMVztBQU0xQkMsbUJBQWdCO0FBTlUsR0FBM0I7O0FBU0FwRyxNQUFJd0QsRUFBSixDQUFPZ0IsWUFBUCxDQUFvQmxFLEVBQXBCLENBQXdCLGFBQXhCLEVBQXVDTixJQUFJZ0YsV0FBM0M7QUFDQSxFQWJEOztBQWVBO0FBQ0FqRixHQUFHQyxJQUFJa0QsSUFBUDtBQUNBLENBMUdDLEVBMEdFckQsTUExR0YsRUEwR1VzRCxNQTFHVixFQTBHa0J0RCxPQUFPMEUsZUExR3pCLENBQUY7OztBQ05BOzs7OztBQUtBOEIsU0FBUzVDLElBQVQsQ0FBYzZDLFNBQWQsR0FBMEJELFNBQVM1QyxJQUFULENBQWM2QyxTQUFkLENBQXdCQyxPQUF4QixDQUFpQyxPQUFqQyxFQUEwQyxJQUExQyxDQUExQjs7O0FDTEEsSUFBTUMsVUFBVSxTQUFoQjtBQUNBaEcsUUFBUUMsR0FBUixDQUFZK0YsT0FBWjtBQUNBM0csT0FBTzRHLGNBQVAsR0FBd0IsRUFBeEI7QUFDRSxXQUFVNUcsTUFBVixFQUFrQkUsQ0FBbEIsRUFBcUJDLEdBQXJCLEVBQTBCMEcsT0FBMUIsRUFBbUM7QUFDbkM7QUFDQTFHLE1BQUlrRCxJQUFKLEdBQVcsWUFBVztBQUNwQmxELFFBQUkyRyxTQUFKO0FBQ0QsR0FGRDtBQUdBM0csTUFBSTJHLFNBQUosR0FBZ0IsWUFBVTtBQUN4Qm5HLFlBQVFDLEdBQVIsQ0FBWSxRQUFaO0FBQ0QsR0FGRDtBQUdGO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQ1YsSUFBR0MsSUFBSWtELElBQVA7QUFDQSxDQWpCQyxFQWlCRXJELE1BakJGLEVBaUJVc0QsTUFqQlYsRUFpQmtCdEQsT0FBTzRHLGNBakJ6QixFQWlCeUNDLE9BakJ6QyxDQUFGOzs7QUNIQTs7Ozs7QUFLQTdHLE9BQU8rRyxhQUFQLEdBQXVCLEVBQXZCO0FBQ0UsV0FBVS9HLE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjs7QUFFNUI7QUFDQUEsS0FBSWtELElBQUosR0FBVyxZQUFXO0FBQ3JCbEQsTUFBSXFELEtBQUo7O0FBRUEsTUFBS3JELElBQUlzRCxpQkFBSixFQUFMLEVBQStCO0FBQzlCdEQsT0FBSXVELFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQXZELEtBQUlxRCxLQUFKLEdBQVksWUFBVztBQUN0QnJELE1BQUl3RCxFQUFKLEdBQVM7QUFDUkMsU0FBTTFELEVBQUcsTUFBSCxDQURFO0FBRVJGLFdBQVFFLEVBQUdGLE1BQUgsQ0FGQTtBQUdSZ0gscUJBQWtCOUcsRUFBRyx1REFBSCxDQUhWO0FBSVIrRyx3QkFBcUIvRyxFQUFHLGtDQUFILENBSmI7QUFLUmdILHNCQUFtQmhILEVBQUcsdUZBQUgsQ0FMWDtBQU1SaUgsdUJBQW9CakgsRUFBRyx1QkFBSDtBQU5aLEdBQVQ7QUFRQSxFQVREOztBQVdBO0FBQ0FDLEtBQUl1RCxVQUFKLEdBQWlCLFlBQVc7QUFDM0J2RCxNQUFJd0QsRUFBSixDQUFPM0QsTUFBUCxDQUFjUyxFQUFkLENBQWtCLE1BQWxCLEVBQTBCTixJQUFJaUgsWUFBOUI7QUFDQWpILE1BQUl3RCxFQUFKLENBQU91RCxpQkFBUCxDQUF5QnpHLEVBQXpCLENBQTZCLE9BQTdCLEVBQXNDTixJQUFJa0gsYUFBMUM7QUFDQWxILE1BQUl3RCxFQUFKLENBQU91RCxpQkFBUCxDQUF5QnpHLEVBQXpCLENBQTZCLGVBQTdCLEVBQThDTixJQUFJbUgsWUFBbEQ7QUFDQW5ILE1BQUl3RCxFQUFKLENBQU93RCxrQkFBUCxDQUEwQjFHLEVBQTFCLENBQThCLGVBQTlCLEVBQStDTixJQUFJb0gsa0JBQW5EO0FBQ0EsRUFMRDs7QUFPQTtBQUNBcEgsS0FBSXNELGlCQUFKLEdBQXdCLFlBQVc7QUFDbEMsU0FBT3RELElBQUl3RCxFQUFKLENBQU9xRCxnQkFBUCxDQUF3QmhELE1BQS9CO0FBQ0EsRUFGRDs7QUFJQTtBQUNBN0QsS0FBSW1ILFlBQUosR0FBbUIsWUFBVzs7QUFFN0I7QUFDQTtBQUNBLE1BQUtwSCxFQUFHLElBQUgsRUFBVXNILEVBQVYsQ0FBYywyQkFBZCxLQUErQyxDQUFFdEgsRUFBRyxJQUFILEVBQVVtRSxRQUFWLENBQW9CLFlBQXBCLENBQXRELEVBQTJGO0FBQzFGbkUsS0FBRyxJQUFILEVBQVU2RSxJQUFWLENBQWdCLGFBQWhCLEVBQWdDVCxXQUFoQyxDQUE2Qyx5QkFBN0M7QUFDQTtBQUVELEVBUkQ7O0FBVUE7QUFDQW5FLEtBQUlzSCxnQkFBSixHQUF1QixVQUFVL0csRUFBVixFQUFlOztBQUVyQztBQUNBLE1BQUtBLEdBQUdnSCxNQUFILEdBQVlyRCxRQUFaLENBQXNCLFlBQXRCLEtBQXdDLENBQUUzRCxHQUFHMkQsUUFBSCxDQUFhLFlBQWIsQ0FBL0MsRUFBNkU7QUFDNUU7QUFDQTs7QUFFRDtBQUNBLE1BQUszRCxHQUFHZ0gsTUFBSCxHQUFZckQsUUFBWixDQUFzQixZQUF0QixLQUF3QzNELEdBQUcyRCxRQUFILENBQWEsWUFBYixDQUE3QyxFQUEyRTtBQUMxRTNELE1BQUc0RCxXQUFILENBQWdCLFlBQWhCLEVBQStCUyxJQUEvQixDQUFxQyxXQUFyQyxFQUFtRFQsV0FBbkQsQ0FBZ0UsYUFBaEUsRUFBZ0YzQyxRQUFoRixDQUEwRixjQUExRjtBQUNBO0FBQ0E7O0FBRUR4QixNQUFJd0QsRUFBSixDQUFPcUQsZ0JBQVAsQ0FBd0JyQixJQUF4QixDQUE4QixZQUFXOztBQUV4QztBQUNBLE9BQUt6RixFQUFHLElBQUgsRUFBVW1FLFFBQVYsQ0FBb0IsYUFBcEIsQ0FBTCxFQUEyQzs7QUFFMUM7QUFDQW5FLE1BQUcsSUFBSCxFQUFVd0gsTUFBVixHQUFtQnBELFdBQW5CLENBQWdDLFlBQWhDLEVBQStDUyxJQUEvQyxDQUFxRCxtQkFBckQsRUFBMkVHLElBQTNFLENBQWlGLGVBQWpGLEVBQWtHLEtBQWxHOztBQUVBO0FBQ0FoRixNQUFHLElBQUgsRUFBVW9FLFdBQVYsQ0FBdUIsYUFBdkIsRUFBdUMzQyxRQUF2QyxDQUFpRCxjQUFqRDtBQUNBO0FBRUQsR0FaRDtBQWFBLEVBMUJEOztBQTRCQTtBQUNBeEIsS0FBSWlILFlBQUosR0FBbUIsWUFBVztBQUM3QmpILE1BQUl3RCxFQUFKLENBQU91RCxpQkFBUCxDQUF5QlMsT0FBekIsQ0FBa0MsMElBQWxDO0FBQ0EsRUFGRDs7QUFJQTtBQUNBeEgsS0FBSWtILGFBQUosR0FBb0IsVUFBVU8sQ0FBVixFQUFjOztBQUVqQyxNQUFJbEgsS0FBS1IsRUFBRyxJQUFILENBQVQ7QUFBQSxNQUFvQjtBQUNuQjJILFlBQVVuSCxHQUFHMEIsUUFBSCxDQUFhLGFBQWIsQ0FEWDtBQUFBLE1BQ3lDO0FBQ3hDMEYsWUFBVTVILEVBQUcwSCxFQUFFekQsTUFBTCxDQUZYLENBRmlDLENBSVA7O0FBRTFCO0FBQ0E7QUFDQSxNQUFLMkQsUUFBUXpELFFBQVIsQ0FBa0IsWUFBbEIsS0FBb0N5RCxRQUFRekQsUUFBUixDQUFrQixrQkFBbEIsQ0FBekMsRUFBa0Y7O0FBRWpGO0FBQ0FsRSxPQUFJc0gsZ0JBQUosQ0FBc0IvRyxFQUF0Qjs7QUFFQSxPQUFLLENBQUVtSCxRQUFReEQsUUFBUixDQUFrQixZQUFsQixDQUFQLEVBQTBDOztBQUV6QztBQUNBbEUsUUFBSTRILFdBQUosQ0FBaUJySCxFQUFqQixFQUFxQm1ILE9BQXJCO0FBRUE7O0FBRUQsVUFBTyxLQUFQO0FBQ0E7QUFFRCxFQXZCRDs7QUF5QkE7QUFDQTFILEtBQUk0SCxXQUFKLEdBQWtCLFVBQVVMLE1BQVYsRUFBa0JHLE9BQWxCLEVBQTRCOztBQUU3QztBQUNBSCxTQUFPL0YsUUFBUCxDQUFpQixZQUFqQixFQUFnQ29ELElBQWhDLENBQXNDLG1CQUF0QyxFQUE0REcsSUFBNUQsQ0FBa0UsZUFBbEUsRUFBbUYsSUFBbkY7O0FBRUE7QUFDQTJDLFVBQVFsRyxRQUFSLENBQWtCLGlDQUFsQjtBQUNBLEVBUEQ7O0FBU0E7QUFDQXhCLEtBQUlvSCxrQkFBSixHQUF5QixZQUFXOztBQUVuQztBQUNBLE1BQUssQ0FBRXJILEVBQUcsSUFBSCxFQUFVbUUsUUFBVixDQUFvQixZQUFwQixDQUFQLEVBQTRDO0FBQzNDbEUsT0FBSXdELEVBQUosQ0FBT3VELGlCQUFQLENBQXlCNUMsV0FBekIsQ0FBc0MsWUFBdEMsRUFBcURTLElBQXJELENBQTJELG1CQUEzRCxFQUFpRkcsSUFBakYsQ0FBdUYsZUFBdkYsRUFBd0csS0FBeEc7QUFDQS9FLE9BQUl3RCxFQUFKLENBQU9xRCxnQkFBUCxDQUF3QjFDLFdBQXhCLENBQXFDLHdCQUFyQztBQUNBbkUsT0FBSXdELEVBQUosQ0FBT0MsSUFBUCxDQUFZcEIsR0FBWixDQUFpQixVQUFqQixFQUE2QixTQUE3QjtBQUNBckMsT0FBSXdELEVBQUosQ0FBT0MsSUFBUCxDQUFZb0UsTUFBWixDQUFvQixZQUFwQjtBQUNBOztBQUVELE1BQUs5SCxFQUFHLElBQUgsRUFBVW1FLFFBQVYsQ0FBb0IsWUFBcEIsQ0FBTCxFQUEwQztBQUN6Q2xFLE9BQUl3RCxFQUFKLENBQU9DLElBQVAsQ0FBWXBCLEdBQVosQ0FBaUIsVUFBakIsRUFBNkIsUUFBN0I7QUFDQXJDLE9BQUl3RCxFQUFKLENBQU9DLElBQVAsQ0FBWW5DLElBQVosQ0FBa0IsWUFBbEIsRUFBZ0MsVUFBVW1HLENBQVYsRUFBYztBQUM3QyxRQUFLLENBQUUxSCxFQUFHMEgsRUFBRXpELE1BQUwsRUFBY0MsT0FBZCxDQUF1QixnQkFBdkIsRUFBMEMsQ0FBMUMsQ0FBUCxFQUFzRDtBQUNyRHdELE9BQUVLLGNBQUY7QUFDQTtBQUNELElBSkQ7QUFLQTtBQUNELEVBbEJEOztBQW9CQTtBQUNBL0gsR0FBR0MsSUFBSWtELElBQVA7QUFFQSxDQTdJQyxFQTZJQ3JELE1BN0lELEVBNklTc0QsTUE3SVQsRUE2SWlCdEQsT0FBTytHLGFBN0l4QixDQUFGOzs7QUNOQTs7Ozs7QUFLQS9HLE9BQU9rSSxRQUFQLEdBQWtCLEVBQWxCO0FBQ0UsV0FBVWxJLE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjs7QUFFNUIsS0FBSWdJLHFCQUFKO0FBQUEsS0FDQ0MsMkJBREQ7QUFBQSxLQUVDQyxnQkFGRDtBQUFBLEtBR0NDLE9BQU85QixTQUFTK0IsYUFBVCxDQUF3QixRQUF4QixDQUhSO0FBQUEsS0FJQ0Msa0JBQWtCaEMsU0FBU2lDLG9CQUFULENBQStCLFFBQS9CLEVBQTBDLENBQTFDLENBSm5CO0FBQUEsS0FLQ0MsV0FMRDs7QUFPQTtBQUNBdkksS0FBSWtELElBQUosR0FBVyxZQUFXO0FBQ3JCbEQsTUFBSXFELEtBQUo7O0FBRUEsTUFBS3JELElBQUlzRCxpQkFBSixFQUFMLEVBQStCO0FBQzlCK0UsbUJBQWdCRyxVQUFoQixDQUEyQkMsWUFBM0IsQ0FBeUNOLElBQXpDLEVBQStDRSxlQUEvQztBQUNBckksT0FBSXVELFVBQUo7QUFDQTtBQUNELEVBUEQ7O0FBU0E7QUFDQXZELEtBQUlxRCxLQUFKLEdBQVksWUFBVztBQUN0QnJELE1BQUl3RCxFQUFKLEdBQVM7QUFDUixXQUFRekQsRUFBRyxNQUFIO0FBREEsR0FBVDtBQUdBLEVBSkQ7O0FBTUE7QUFDQUMsS0FBSXNELGlCQUFKLEdBQXdCLFlBQVc7QUFDbEMsU0FBT3ZELEVBQUcsZ0JBQUgsRUFBc0I4RCxNQUE3QjtBQUNBLEVBRkQ7O0FBSUE7QUFDQTdELEtBQUl1RCxVQUFKLEdBQWlCLFlBQVc7O0FBRTNCO0FBQ0F2RCxNQUFJd0QsRUFBSixDQUFPQyxJQUFQLENBQVluRCxFQUFaLENBQWdCLGtCQUFoQixFQUFvQyxnQkFBcEMsRUFBc0ROLElBQUkwSSxTQUExRDs7QUFFQTtBQUNBMUksTUFBSXdELEVBQUosQ0FBT0MsSUFBUCxDQUFZbkQsRUFBWixDQUFnQixrQkFBaEIsRUFBb0MsUUFBcEMsRUFBOENOLElBQUkySSxVQUFsRDs7QUFFQTtBQUNBM0ksTUFBSXdELEVBQUosQ0FBT0MsSUFBUCxDQUFZbkQsRUFBWixDQUFnQixTQUFoQixFQUEyQk4sSUFBSTRJLFdBQS9COztBQUVBO0FBQ0E1SSxNQUFJd0QsRUFBSixDQUFPQyxJQUFQLENBQVluRCxFQUFaLENBQWdCLGtCQUFoQixFQUFvQyxnQkFBcEMsRUFBc0ROLElBQUk2SSxpQkFBMUQ7O0FBRUE7QUFDQTdJLE1BQUl3RCxFQUFKLENBQU9DLElBQVAsQ0FBWW5ELEVBQVosQ0FBZ0IsU0FBaEIsRUFBMkJOLElBQUk4SSxpQkFBL0I7QUFFQSxFQWpCRDs7QUFtQkE7QUFDQTlJLEtBQUkwSSxTQUFKLEdBQWdCLFlBQVc7O0FBRTFCO0FBQ0FWLGlCQUFlakksRUFBRyxJQUFILENBQWY7O0FBRUE7QUFDQSxNQUFJZ0osU0FBU2hKLEVBQUdBLEVBQUcsSUFBSCxFQUFVaUosSUFBVixDQUFnQixRQUFoQixDQUFILENBQWI7O0FBRUE7QUFDQUQsU0FBT3ZILFFBQVAsQ0FBaUIsWUFBakI7O0FBRUE7QUFDQXhCLE1BQUl3RCxFQUFKLENBQU9DLElBQVAsQ0FBWWpDLFFBQVosQ0FBc0IsWUFBdEI7O0FBRUE7QUFDQTtBQUNBO0FBQ0F5Ryx1QkFBcUJjLE9BQU9uRSxJQUFQLENBQWEsdUJBQWIsQ0FBckI7O0FBRUE7QUFDQSxNQUFLLElBQUlxRCxtQkFBbUJwRSxNQUE1QixFQUFxQzs7QUFFcEM7QUFDQW9FLHNCQUFtQixDQUFuQixFQUFzQmdCLEtBQXRCO0FBQ0E7QUFFRCxFQTFCRDs7QUE0QkE7QUFDQWpKLEtBQUkySSxVQUFKLEdBQWlCLFlBQVc7O0FBRTNCO0FBQ0EsTUFBSUksU0FBU2hKLEVBQUdBLEVBQUcsdUJBQUgsRUFBNkJpSixJQUE3QixDQUFtQyxRQUFuQyxDQUFILENBQWI7OztBQUVDO0FBQ0FFLFlBQVVILE9BQU9uRSxJQUFQLENBQWEsUUFBYixDQUhYOztBQUtBO0FBQ0EsTUFBS3NFLFFBQVFyRixNQUFiLEVBQXNCOztBQUVyQjtBQUNBLE9BQUlzRixNQUFNRCxRQUFRbkUsSUFBUixDQUFjLEtBQWQsQ0FBVjs7QUFFQTtBQUNBO0FBQ0EsT0FBSyxDQUFFb0UsSUFBSUMsUUFBSixDQUFjLGVBQWQsQ0FBUCxFQUF5Qzs7QUFFeEM7QUFDQUYsWUFBUW5FLElBQVIsQ0FBYyxLQUFkLEVBQXFCLEVBQXJCLEVBQTBCQSxJQUExQixDQUFnQyxLQUFoQyxFQUF1Q29FLEdBQXZDO0FBQ0EsSUFKRCxNQUlPOztBQUVOO0FBQ0FqQixZQUFRbUIsU0FBUjtBQUNBO0FBQ0Q7O0FBRUQ7QUFDQU4sU0FBTzVFLFdBQVAsQ0FBb0IsWUFBcEI7O0FBRUE7QUFDQW5FLE1BQUl3RCxFQUFKLENBQU9DLElBQVAsQ0FBWVUsV0FBWixDQUF5QixZQUF6Qjs7QUFFQTtBQUNBNkQsZUFBYWlCLEtBQWI7QUFFQSxFQXBDRDs7QUFzQ0E7QUFDQWpKLEtBQUk0SSxXQUFKLEdBQWtCLFVBQVU3RSxLQUFWLEVBQWtCO0FBQ25DLE1BQUssT0FBT0EsTUFBTXVGLE9BQWxCLEVBQTRCO0FBQzNCdEosT0FBSTJJLFVBQUo7QUFDQTtBQUNELEVBSkQ7O0FBTUE7QUFDQTNJLEtBQUk2SSxpQkFBSixHQUF3QixVQUFVOUUsS0FBVixFQUFrQjs7QUFFekM7QUFDQSxNQUFLLENBQUVoRSxFQUFHZ0UsTUFBTUMsTUFBVCxFQUFrQkMsT0FBbEIsQ0FBMkIsS0FBM0IsRUFBbUNDLFFBQW5DLENBQTZDLGNBQTdDLENBQVAsRUFBdUU7QUFDdEVsRSxPQUFJMkksVUFBSjtBQUNBO0FBQ0QsRUFORDs7QUFRQTtBQUNBM0ksS0FBSThJLGlCQUFKLEdBQXdCLFVBQVUvRSxLQUFWLEVBQWtCOztBQUV6QztBQUNBLE1BQUssTUFBTUEsTUFBTXdGLEtBQVosSUFBcUIsSUFBSXhKLEVBQUcsYUFBSCxFQUFtQjhELE1BQWpELEVBQTBEO0FBQ3pELE9BQUkyRixXQUFXekosRUFBRyxRQUFILENBQWY7QUFBQSxPQUNDMEosYUFBYXhCLG1CQUFtQnlCLEtBQW5CLENBQTBCRixRQUExQixDQURkOztBQUdBLE9BQUssTUFBTUMsVUFBTixJQUFvQjFGLE1BQU00RixRQUEvQixFQUEwQzs7QUFFekM7QUFDQTFCLHVCQUFvQkEsbUJBQW1CcEUsTUFBbkIsR0FBNEIsQ0FBaEQsRUFBb0RvRixLQUFwRDtBQUNBbEYsVUFBTStELGNBQU47QUFDQSxJQUxELE1BS08sSUFBSyxDQUFFL0QsTUFBTTRGLFFBQVIsSUFBb0JGLGVBQWV4QixtQkFBbUJwRSxNQUFuQixHQUE0QixDQUFwRSxFQUF3RTs7QUFFOUU7QUFDQW9FLHVCQUFtQixDQUFuQixFQUFzQmdCLEtBQXRCO0FBQ0FsRixVQUFNK0QsY0FBTjtBQUNBO0FBQ0Q7QUFDRCxFQW5CRDs7QUFxQkE7QUFDQTlILEtBQUk0Six1QkFBSixHQUE4QixZQUFXO0FBQ3hDLE1BQUliLFNBQVNoSixFQUFHLFdBQUgsQ0FBYjtBQUFBLE1BQ0M4SixZQUFZZCxPQUFPbkUsSUFBUCxDQUFhLFFBQWIsRUFBd0JHLElBQXhCLENBQThCLElBQTlCLENBRGI7O0FBR0FtRCxZQUFVLElBQUlLLEdBQUd1QixNQUFQLENBQWVELFNBQWYsRUFBMEI7QUFDbkNFLFdBQVE7QUFDUCxlQUFXL0osSUFBSWdLLGFBRFI7QUFFUCxxQkFBaUJoSyxJQUFJaUs7QUFGZDtBQUQyQixHQUExQixDQUFWO0FBTUEsRUFWRDs7QUFZQTtBQUNBakssS0FBSWdLLGFBQUosR0FBb0IsWUFBVyxDQUM5QixDQUREOztBQUdBO0FBQ0FoSyxLQUFJaUssbUJBQUosR0FBMEIsWUFBVzs7QUFFcEM7QUFDQWxLLElBQUdnRSxNQUFNQyxNQUFOLENBQWFrRyxDQUFoQixFQUFvQmpHLE9BQXBCLENBQTZCLFFBQTdCLEVBQXdDVyxJQUF4QyxDQUE4Qyx1QkFBOUMsRUFBd0V1RixLQUF4RSxHQUFnRmxCLEtBQWhGO0FBQ0EsRUFKRDs7QUFPQTtBQUNBbEosR0FBR0MsSUFBSWtELElBQVA7QUFDQSxDQXhMQyxFQXdMQ3JELE1BeExELEVBd0xTc0QsTUF4TFQsRUF3TGlCdEQsT0FBT2tJLFFBeEx4QixDQUFGOzs7QUNOQTs7Ozs7QUFLQWxJLE9BQU91SyxvQkFBUCxHQUE4QixFQUE5QjtBQUNFLFdBQVV2SyxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMkI7O0FBRTVCO0FBQ0FBLEtBQUlrRCxJQUFKLEdBQVcsWUFBVztBQUNyQmxELE1BQUlxRCxLQUFKOztBQUVBLE1BQUtyRCxJQUFJc0QsaUJBQUosRUFBTCxFQUErQjtBQUM5QnRELE9BQUl1RCxVQUFKO0FBQ0E7QUFDRCxFQU5EOztBQVFBO0FBQ0F2RCxLQUFJcUQsS0FBSixHQUFZLFlBQVc7QUFDdEJyRCxNQUFJd0QsRUFBSixHQUFTO0FBQ1IzRCxXQUFRRSxFQUFHRixNQUFILENBREE7QUFFUmdILHFCQUFrQjlHLEVBQUcsNEJBQUgsQ0FGVjtBQUdSZ0gsc0JBQW1CaEgsRUFBRyw0Q0FBSDtBQUhYLEdBQVQ7QUFLQSxFQU5EOztBQVFBO0FBQ0FDLEtBQUl1RCxVQUFKLEdBQWlCLFlBQVc7QUFDM0J2RCxNQUFJd0QsRUFBSixDQUFPM0QsTUFBUCxDQUFjUyxFQUFkLENBQWtCLE1BQWxCLEVBQTBCTixJQUFJaUgsWUFBOUI7QUFDQWpILE1BQUl3RCxFQUFKLENBQU91RCxpQkFBUCxDQUF5Qm5DLElBQXpCLENBQStCLEdBQS9CLEVBQXFDdEUsRUFBckMsQ0FBeUMsa0JBQXpDLEVBQTZETixJQUFJcUssV0FBakU7QUFDQSxFQUhEOztBQUtBO0FBQ0FySyxLQUFJc0QsaUJBQUosR0FBd0IsWUFBVztBQUNsQyxTQUFPdEQsSUFBSXdELEVBQUosQ0FBT3FELGdCQUFQLENBQXdCaEQsTUFBL0I7QUFDQSxFQUZEOztBQUlBO0FBQ0E3RCxLQUFJaUgsWUFBSixHQUFtQixZQUFXO0FBQzdCakgsTUFBSXdELEVBQUosQ0FBT3VELGlCQUFQLENBQXlCbkMsSUFBekIsQ0FBK0IsS0FBL0IsRUFBdUMwRixNQUF2QyxDQUErQyxxREFBL0M7QUFDQSxFQUZEOztBQUlBO0FBQ0F0SyxLQUFJcUssV0FBSixHQUFrQixZQUFXO0FBQzVCdEssSUFBRyxJQUFILEVBQVVrRSxPQUFWLENBQW1CLDJCQUFuQixFQUFpREgsV0FBakQsQ0FBOEQsT0FBOUQ7QUFDQSxFQUZEOztBQUlBO0FBQ0EvRCxHQUFHQyxJQUFJa0QsSUFBUDtBQUVBLENBNUNDLEVBNENDckQsTUE1Q0QsRUE0Q1NzRCxNQTVDVCxFQTRDaUJ0RCxPQUFPdUssb0JBNUN4QixDQUFGOzs7QUNOQTs7Ozs7QUFLQXZLLE9BQU8wSyxZQUFQLEdBQXNCLEVBQXRCO0FBQ0UsV0FBVTFLLE1BQVYsRUFBa0JFLENBQWxCLEVBQXFCQyxHQUFyQixFQUEyQjs7QUFFNUI7QUFDQUEsS0FBSWtELElBQUosR0FBVyxZQUFXO0FBQ3JCbEQsTUFBSXFELEtBQUo7O0FBRUEsTUFBS3JELElBQUlzRCxpQkFBSixFQUFMLEVBQStCO0FBQzlCdEQsT0FBSXVELFVBQUo7QUFDQTtBQUNELEVBTkQ7O0FBUUE7QUFDQXZELEtBQUlxRCxLQUFKLEdBQVksWUFBVztBQUN0QnJELE1BQUl3RCxFQUFKLEdBQVM7QUFDUkMsU0FBTTFELEVBQUcsTUFBSCxDQURFO0FBRVJ5SyxtQkFBZ0J6SyxFQUFHLG1CQUFILENBRlI7QUFHUmlILHVCQUFvQmpILEVBQUcsdUJBQUgsQ0FIWjtBQUlSMEssa0JBQWUxSyxFQUFHLGtCQUFILENBSlA7QUFLUjJLLG9CQUFpQjNLLEVBQUcsb0JBQUg7QUFMVCxHQUFUO0FBT0EsRUFSRDs7QUFVQTtBQUNBQyxLQUFJdUQsVUFBSixHQUFpQixZQUFXO0FBQzNCdkQsTUFBSXdELEVBQUosQ0FBT0MsSUFBUCxDQUFZbkQsRUFBWixDQUFnQixTQUFoQixFQUEyQk4sSUFBSTRJLFdBQS9CO0FBQ0E1SSxNQUFJd0QsRUFBSixDQUFPZ0gsY0FBUCxDQUFzQmxLLEVBQXRCLENBQTBCLE9BQTFCLEVBQW1DTixJQUFJMkssY0FBdkM7QUFDQTNLLE1BQUl3RCxFQUFKLENBQU9pSCxhQUFQLENBQXFCbkssRUFBckIsQ0FBeUIsT0FBekIsRUFBa0NOLElBQUk0SyxlQUF0QztBQUNBNUssTUFBSXdELEVBQUosQ0FBT2tILGVBQVAsQ0FBdUJwSyxFQUF2QixDQUEyQixPQUEzQixFQUFvQ04sSUFBSTJLLGNBQXhDO0FBQ0EsRUFMRDs7QUFPQTtBQUNBM0ssS0FBSXNELGlCQUFKLEdBQXdCLFlBQVc7QUFDbEMsU0FBT3RELElBQUl3RCxFQUFKLENBQU93RCxrQkFBUCxDQUEwQm5ELE1BQWpDO0FBQ0EsRUFGRDs7QUFJQTtBQUNBN0QsS0FBSTRLLGVBQUosR0FBc0IsWUFBVzs7QUFFaEMsTUFBSyxXQUFXN0ssRUFBRyxJQUFILEVBQVVnRixJQUFWLENBQWdCLGVBQWhCLENBQWhCLEVBQW9EO0FBQ25EL0UsT0FBSTJLLGNBQUo7QUFDQSxHQUZELE1BRU87QUFDTjNLLE9BQUk2SyxhQUFKO0FBQ0E7QUFFRCxFQVJEOztBQVVBO0FBQ0E3SyxLQUFJNkssYUFBSixHQUFvQixZQUFXO0FBQzlCN0ssTUFBSXdELEVBQUosQ0FBT3dELGtCQUFQLENBQTBCeEYsUUFBMUIsQ0FBb0MsWUFBcEM7QUFDQXhCLE1BQUl3RCxFQUFKLENBQU9pSCxhQUFQLENBQXFCakosUUFBckIsQ0FBK0IsWUFBL0I7QUFDQXhCLE1BQUl3RCxFQUFKLENBQU9rSCxlQUFQLENBQXVCbEosUUFBdkIsQ0FBaUMsWUFBakM7O0FBRUF4QixNQUFJd0QsRUFBSixDQUFPaUgsYUFBUCxDQUFxQjFGLElBQXJCLENBQTJCLGVBQTNCLEVBQTRDLElBQTVDO0FBQ0EvRSxNQUFJd0QsRUFBSixDQUFPd0Qsa0JBQVAsQ0FBMEJqQyxJQUExQixDQUFnQyxhQUFoQyxFQUErQyxLQUEvQzs7QUFFQS9FLE1BQUl3RCxFQUFKLENBQU93RCxrQkFBUCxDQUEwQnBDLElBQTFCLENBQWdDLFFBQWhDLEVBQTJDdUYsS0FBM0MsR0FBbURsQixLQUFuRDtBQUNBLEVBVEQ7O0FBV0E7QUFDQWpKLEtBQUkySyxjQUFKLEdBQXFCLFlBQVc7QUFDL0IzSyxNQUFJd0QsRUFBSixDQUFPd0Qsa0JBQVAsQ0FBMEI3QyxXQUExQixDQUF1QyxZQUF2QztBQUNBbkUsTUFBSXdELEVBQUosQ0FBT2lILGFBQVAsQ0FBcUJ0RyxXQUFyQixDQUFrQyxZQUFsQztBQUNBbkUsTUFBSXdELEVBQUosQ0FBT2tILGVBQVAsQ0FBdUJ2RyxXQUF2QixDQUFvQyxZQUFwQzs7QUFFQW5FLE1BQUl3RCxFQUFKLENBQU9pSCxhQUFQLENBQXFCMUYsSUFBckIsQ0FBMkIsZUFBM0IsRUFBNEMsS0FBNUM7QUFDQS9FLE1BQUl3RCxFQUFKLENBQU93RCxrQkFBUCxDQUEwQmpDLElBQTFCLENBQWdDLGFBQWhDLEVBQStDLElBQS9DOztBQUVBL0UsTUFBSXdELEVBQUosQ0FBT2lILGFBQVAsQ0FBcUJ4QixLQUFyQjtBQUNBLEVBVEQ7O0FBV0E7QUFDQWpKLEtBQUk0SSxXQUFKLEdBQWtCLFVBQVU3RSxLQUFWLEVBQWtCO0FBQ25DLE1BQUssT0FBT0EsTUFBTXVGLE9BQWxCLEVBQTRCO0FBQzNCdEosT0FBSTJLLGNBQUo7QUFDQTtBQUNELEVBSkQ7O0FBTUE7QUFDQTVLLEdBQUdDLElBQUlrRCxJQUFQO0FBRUEsQ0FoRkMsRUFnRkNyRCxNQWhGRCxFQWdGU3NELE1BaEZULEVBZ0ZpQnRELE9BQU8wSyxZQWhGeEIsQ0FBRjs7O0FDTkE7Ozs7Ozs7QUFPRSxhQUFXO0FBQ1osS0FBSU8sV0FBVyxDQUFDLENBQUQsR0FBS0MsVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLFFBQTNDLENBQXBCO0FBQUEsS0FDQ0MsVUFBVSxDQUFDLENBQUQsR0FBS0osVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLE9BQTNDLENBRGhCO0FBQUEsS0FFQ0UsT0FBTyxDQUFDLENBQUQsR0FBS0wsVUFBVUMsU0FBVixDQUFvQkMsV0FBcEIsR0FBa0NDLE9BQWxDLENBQTJDLE1BQTNDLENBRmI7O0FBSUEsS0FBSyxDQUFFSixZQUFZSyxPQUFaLElBQXVCQyxJQUF6QixLQUFtQy9FLFNBQVNnRixjQUE1QyxJQUE4RHhMLE9BQU95TCxnQkFBMUUsRUFBNkY7QUFDNUZ6TCxTQUFPeUwsZ0JBQVAsQ0FBeUIsWUFBekIsRUFBdUMsWUFBVztBQUNqRCxPQUFJQyxLQUFLQyxTQUFTQyxJQUFULENBQWNDLFNBQWQsQ0FBeUIsQ0FBekIsQ0FBVDtBQUFBLE9BQ0NDLE9BREQ7O0FBR0EsT0FBSyxDQUFJLGVBQUYsQ0FBb0JDLElBQXBCLENBQTBCTCxFQUExQixDQUFQLEVBQXdDO0FBQ3ZDO0FBQ0E7O0FBRURJLGFBQVV0RixTQUFTZ0YsY0FBVCxDQUF5QkUsRUFBekIsQ0FBVjs7QUFFQSxPQUFLSSxPQUFMLEVBQWU7QUFDZCxRQUFLLENBQUksdUNBQUYsQ0FBNENDLElBQTVDLENBQWtERCxRQUFRRSxPQUExRCxDQUFQLEVBQTZFO0FBQzVFRixhQUFRRyxRQUFSLEdBQW1CLENBQUMsQ0FBcEI7QUFDQTs7QUFFREgsWUFBUTFDLEtBQVI7QUFDQTtBQUNELEdBakJELEVBaUJHLEtBakJIO0FBa0JBO0FBQ0QsQ0F6QkMsR0FBRjs7O0FDUEE7Ozs7O0FBS0FwSixPQUFPa00sY0FBUCxHQUF3QixFQUF4QjtBQUNFLFdBQVVsTSxNQUFWLEVBQWtCRSxDQUFsQixFQUFxQkMsR0FBckIsRUFBMkI7O0FBRTVCO0FBQ0FBLEtBQUlrRCxJQUFKLEdBQVcsWUFBVztBQUNyQmxELE1BQUlxRCxLQUFKO0FBQ0FyRCxNQUFJdUQsVUFBSjtBQUNBLEVBSEQ7O0FBS0E7QUFDQXZELEtBQUlxRCxLQUFKLEdBQVksWUFBVztBQUN0QnJELE1BQUl3RCxFQUFKLEdBQVM7QUFDUixhQUFVekQsRUFBR0YsTUFBSCxDQURGO0FBRVIsV0FBUUUsRUFBR3NHLFNBQVM1QyxJQUFaO0FBRkEsR0FBVDtBQUlBLEVBTEQ7O0FBT0E7QUFDQXpELEtBQUl1RCxVQUFKLEdBQWlCLFlBQVc7QUFDM0J2RCxNQUFJd0QsRUFBSixDQUFPM0QsTUFBUCxDQUFjbU0sSUFBZCxDQUFvQmhNLElBQUlpTSxZQUF4QjtBQUNBLEVBRkQ7O0FBSUE7QUFDQWpNLEtBQUlpTSxZQUFKLEdBQW1CLFlBQVc7QUFDN0JqTSxNQUFJd0QsRUFBSixDQUFPQyxJQUFQLENBQVlqQyxRQUFaLENBQXNCLE9BQXRCO0FBQ0EsRUFGRDs7QUFJQTtBQUNBekIsR0FBR0MsSUFBSWtELElBQVA7QUFDQSxDQTVCQyxFQTRCQ3JELE1BNUJELEVBNEJTc0QsTUE1QlQsRUE0QmlCdEQsT0FBT2tNLGNBNUJ4QixDQUFGIiwiZmlsZSI6InByb2plY3QuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKipcbiAqIENlbnRlciBNZW51IGxvZ29cbiAqXG4gKiBAYXV0aG9yIEhlY3RvciBPdmFsbGVzXG4gKi9cbkJhcmJhLlBqYXguc3RhcnQoKTtcbndpbmRvdy5DZW50ZXJlZE1lbnUgPSB7fTtcbiggZnVuY3Rpb24oIHdpbmRvdywgJCwgYXBwICkge1xuICB2YXIgYXZlcmFnZU1lbnVJdGVtcyA9ICQoXCIjY2VudGVyZWQtbWVudSA+IGxpXCIpLnNpemUoKS8yLTE7XG4gICQoXCIuc2l0ZS1oZWFkZXItY2VudGVyZWQgLnNpdGUtYnJhbmRpbmdcIikuaW5zZXJ0QWZ0ZXIoXCIjY2VudGVyZWQtbWVudSBsaTpudGgoXCIrIGF2ZXJhZ2VNZW51SXRlbXMgK1wiKVwiKTtcbiAgdmFyIGxhc3RFbGVtZW50Q2xpY2tlZDtcbiAgQmFyYmEuRGlzcGF0Y2hlci5vbignbGlua0NsaWNrZWQnLCBmdW5jdGlvbihlbCkge1xuICBsYXN0RWxlbWVudENsaWNrZWQgPSBlbDtcbiAgY29uc29sZS5sb2coZWwpO1xufSk7XG5CYXJiYS5EaXNwYXRjaGVyLm9uKCduZXdQYWdlUmVhZHknLCBmdW5jdGlvbihjdXJyZW50U3RhdHVzLCBvbGRTdGF0dXMsIGNvbnRhaW5lcikge1xuY29uc29sZS5sb2coXCJuZXcgcGFnZSByZWFkeVwiKTtcbn0pO1xuICB2YXIgRmFkZVRyYW5zaXRpb24gPSBCYXJiYS5CYXNlVHJhbnNpdGlvbi5leHRlbmQoe1xuICAgIHN0YXJ0OiBmdW5jdGlvbigpIHtcbiAgICAgIC8qKlxuICAgICAgICogVGhpcyBmdW5jdGlvbiBpcyBhdXRvbWF0aWNhbGx5IGNhbGxlZCBhcyBzb29uIHRoZSBUcmFuc2l0aW9uIHN0YXJ0c1xuICAgICAgICogdGhpcy5uZXdDb250YWluZXJMb2FkaW5nIGlzIGEgUHJvbWlzZSBmb3IgdGhlIGxvYWRpbmcgb2YgdGhlIG5ldyBjb250YWluZXJcbiAgICAgICAqIChCYXJiYS5qcyBhbHNvIGNvbWVzIHdpdGggYW4gaGFuZHkgUHJvbWlzZSBwb2x5ZmlsbCEpXG4gICAgICAgKi9cblxuICAgICAgLy8gQXMgc29vbiB0aGUgbG9hZGluZyBpcyBmaW5pc2hlZCBhbmQgdGhlIG9sZCBwYWdlIGlzIGZhZGVkIG91dCwgbGV0J3MgZmFkZSB0aGUgbmV3IHBhZ2VcbiAgICAgIFByb21pc2VcbiAgICAgICAgLmFsbChbdGhpcy5uZXdDb250YWluZXJMb2FkaW5nLCB0aGlzLmZhZGVPdXQoKV0pXG4gICAgICAgIC50aGVuKHRoaXMuZmFkZUluLmJpbmQodGhpcykpO1xuICAgICAgICAgIC8vICBjb25zb2xlLmxvZygkKFwiLmhlcm8taW1hZ2VcIikpO1xuICAgIH0sXG5cbiAgICBmYWRlT3V0OiBmdW5jdGlvbigpIHtcbiAgICAgIC8qKlxuICAgICAgICogdGhpcy5vbGRDb250YWluZXIgaXMgdGhlIEhUTUxFbGVtZW50IG9mIHRoZSBvbGQgQ29udGFpbmVyXG4gICAgICAgKi9cbiAgICAgIHJldHVybiAkKHRoaXMub2xkQ29udGFpbmVyKS5hZGRDbGFzcygnaXMtY2hhbmdpbmctcGFnZScpLmRlbGF5KDEyMDApLnByb21pc2UoKTtcbiAgICB9LFxuXG4gICAgZmFkZUluOiBmdW5jdGlvbigpIHtcbiAgICAgIC8qKlxuICAgICAgICogdGhpcy5uZXdDb250YWluZXIgaXMgdGhlIEhUTUxFbGVtZW50IG9mIHRoZSBuZXcgQ29udGFpbmVyXG4gICAgICAgKiBBdCB0aGlzIHN0YWdlIG5ld0NvbnRhaW5lciBpcyBvbiB0aGUgRE9NIChpbnNpZGUgb3VyICNiYXJiYS1jb250YWluZXIgYW5kIHdpdGggdmlzaWJpbGl0eTogaGlkZGVuKVxuICAgICAgICogUGxlYXNlIG5vdGUsIG5ld0NvbnRhaW5lciBpcyBhdmFpbGFibGUganVzdCBhZnRlciBuZXdDb250YWluZXJMb2FkaW5nIGlzIHJlc29sdmVkIVxuICAgICAgICovXG5cbiAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICB2YXIgJGVsID0gJCh0aGlzLm5ld0NvbnRhaW5lcik7XG4gICAgICB2YXIgaGVyb0ltYWdlID0gJGVsLmNvbnRleHQuY2hpbGROb2Rlc1sxXS5jaGlsZE5vZGVzWzFdO1xuICAgICAgIGNvbnNvbGUubG9nKCRlbC5jb250ZXh0LmNoaWxkTm9kZXNbMV0uY2hpbGROb2Rlc1sxXSk7XG4gICAgICAgLy8gY29uc29sZS5sb2coQmFyYmEuUGpheC5Eb20uZGF0YU5hbWVzcGFjZSk7XG4gICAgICAgdmFyIGNoaWxkcmVuID0gJGVsLmNvbnRleHQuY2hpbGROb2Rlc1sxXTtcbi8vY2hpbGRyZW4uZm9yRWFjaChmdW5jdGlvbihpdGVtKXtcbiAgICAvL2NvbnNvbGUubG9nKGl0ZW0pO1xuLy8gfSk7XG4gICAgICAvL2NvbnNvbGUubG9nKCQoXCIjcGFnZVwiKSk7XG4gICAgICAvLyAkKFwiI3BhZ2VcIikuY3NzKFwidHJhbnNmb3JtXCIsXCJ0cmFuc2xhdGUoNTAlKVwiKTtcbiAgICAgIC8vJCh0aGlzLm9sZENvbnRhaW5lcikuaGlkZSgpO1xuICAgICAgY29uc29sZS5sb2coVHdlZW5MaXRlKTtcblxuICAgIGNvbnNvbGUubG9nKFR3ZWVuTGl0ZS5mcm9tKGhlcm9JbWFnZSwgMS4zLCB7XG4gICAgICAgIHRvcDogXCIxMjNweFwiLFxuICAgICAgfSkpO1xuICAgICAgJGVsLmNzcyh7XG4gICAgICAgIHZpc2liaWxpdHkgOiAndmlzaWJsZScsXG4gICAgICAgIG9wYWNpdHkgOiAwXG4gICAgICB9KTtcblxuICAgICAgJGVsLmFuaW1hdGUoeyBvcGFjaXR5OiAxICB9LCA0MDAsIGZ1bmN0aW9uKCkge1xuICAgICAgICAvKipcbiAgICAgICAgICogRG8gbm90IGZvcmdldCB0byBjYWxsIC5kb25lKCkgYXMgc29vbiB5b3VyIHRyYW5zaXRpb24gaXMgZmluaXNoZWQhXG4gICAgICAgICAqIC5kb25lKCkgd2lsbCBhdXRvbWF0aWNhbGx5IHJlbW92ZSBmcm9tIHRoZSBET00gdGhlIG9sZCBDb250YWluZXJcbiAgICAgICAgICovXG5cbiAgICAgICAgX3RoaXMuZG9uZSgpO1xuICAgICAgfSk7XG4gICAgfVxuICB9KTtcblxuICAvKipcbiAgICogTmV4dCBzdGVwLCB5b3UgaGF2ZSB0byB0ZWxsIEJhcmJhIHRvIHVzZSB0aGUgbmV3IFRyYW5zaXRpb25cbiAgICovXG5cbiAgQmFyYmEuUGpheC5nZXRUcmFuc2l0aW9uID0gZnVuY3Rpb24oKSB7XG4gICAgLyoqXG4gICAgICogSGVyZSB5b3UgY2FuIHVzZSB5b3VyIG93biBsb2dpYyFcbiAgICAgKiBGb3IgZXhhbXBsZSB5b3UgY2FuIHVzZSBkaWZmZXJlbnQgVHJhbnNpdGlvbiBiYXNlZCBvbiB0aGUgY3VycmVudCBwYWdlIG9yIGxpbmsuLi5cbiAgICAgKi9cblxuICAgIHJldHVybiBGYWRlVHJhbnNpdGlvbjtcbiAgfTtcbiAgdmFyIEhvbWVwYWdlID0gQmFyYmEuQmFzZVZpZXcuZXh0ZW5kKHtcbiAgbmFtZXNwYWNlOiAnaG9tZScsXG4gIG9uRW50ZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgLy8gVGhlIG5ldyBDb250YWluZXIgaXMgcmVhZHkgYW5kIGF0dGFjaGVkIHRvIHRoZSBET00uXG4gICAgICBjb25zb2xlLmxvZyhcImVudGVyIGhvbWVcIik7XG4gIH0sXG4gIG9uRW50ZXJDb21wbGV0ZWQ6IGZ1bmN0aW9uKCkge1xuICAgICAgLy8gVGhlIFRyYW5zaXRpb24gaGFzIGp1c3QgZmluaXNoZWQuXG4gIH0sXG4gIG9uTGVhdmU6IGZ1bmN0aW9uKCkge1xuICAgICAgLy8gQSBuZXcgVHJhbnNpdGlvbiB0b3dhcmQgYSBuZXcgcGFnZSBoYXMganVzdCBzdGFydGVkLlxuICB9LFxuICBvbkxlYXZlQ29tcGxldGVkOiBmdW5jdGlvbigpIHtcbiAgICAgIC8vIFRoZSBDb250YWluZXIgaGFzIGp1c3QgYmVlbiByZW1vdmVkIGZyb20gdGhlIERPTS5cbiAgfVxufSk7XG5cbi8vIERvbid0IGZvcmdldCB0byBpbml0IHRoZSB2aWV3IVxuSG9tZXBhZ2UuaW5pdCgpO1xuXG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy5DZW50ZXJlZE1lbnUgKSApXG4iLCIvKipcbiAqIFNob3cvSGlkZSB0aGUgU2VhcmNoIEZvcm0gaW4gdGhlIGhlYWRlci5cbiAqXG4gKiBAYXV0aG9yIENvcmV5IENvbGxpbnNcbiAqL1xud2luZG93LlNob3dIaWRlU2VhcmNoRm9ybSA9IHt9O1xuKCBmdW5jdGlvbiggd2luZG93LCAkLCBhcHAgKSB7XG5cblx0Ly8gQ29uc3RydWN0b3Jcblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5nc1xuXHRhcHAuY2FjaGUgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMgPSB7XG5cdFx0XHR3aW5kb3c6ICQoIHdpbmRvdyApLFxuXHRcdFx0Ym9keTogJCggJ2JvZHknICksXG5cdFx0XHRoZWFkZXJTZWFyY2hGb3JtOiAkKCAnLnNpdGUtaGVhZGVyLWFjdGlvbiAuY3RhLWJ1dHRvbicgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLmhlYWRlclNlYXJjaEZvcm0ub24oICdrZXl1cCB0b3VjaHN0YXJ0IGNsaWNrJywgYXBwLnNob3dIaWRlU2VhcmNoRm9ybSApO1xuXHRcdGFwcC4kYy5ib2R5Lm9uKCAna2V5dXAgdG91Y2hzdGFydCBjbGljaycsIGFwcC5oaWRlU2VhcmNoRm9ybSApO1xuXHR9O1xuXG5cdC8vIERvIHdlIG1lZXQgdGhlIHJlcXVpcmVtZW50cz9cblx0YXBwLm1lZXRzUmVxdWlyZW1lbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIGFwcC4kYy5oZWFkZXJTZWFyY2hGb3JtLmxlbmd0aDtcblx0fTtcblxuXHQvLyBBZGRzIHRoZSB0b2dnbGUgY2xhc3MgZm9yIHRoZSBzZWFyY2ggZm9ybS5cblx0YXBwLnNob3dIaWRlU2VhcmNoRm9ybSA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy5ib2R5LnRvZ2dsZUNsYXNzKCAnc2VhcmNoLWZvcm0tdmlzaWJsZScgKTtcblx0fTtcblxuXHQvLyBIaWRlcyB0aGUgc2VhcmNoIGZvcm0gaWYgd2UgY2xpY2sgb3V0c2lkZSBvZiBpdHMgY29udGFpbmVyLlxuXHRhcHAuaGlkZVNlYXJjaEZvcm0gPSBmdW5jdGlvbiggZXZlbnQgKSB7XG5cblx0XHRpZiAoICEgJCggZXZlbnQudGFyZ2V0ICkucGFyZW50cyggJ2RpdicgKS5oYXNDbGFzcyggJ3NpdGUtaGVhZGVyLWFjdGlvbicgKSApIHtcblx0XHRcdGFwcC4kYy5ib2R5LnJlbW92ZUNsYXNzKCAnc2VhcmNoLWZvcm0tdmlzaWJsZScgKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gRW5nYWdlXG5cdCQoIGFwcC5pbml0ICk7XG5cbn0gKCB3aW5kb3csIGpRdWVyeSwgd2luZG93LlNob3dIaWRlU2VhcmNoRm9ybSApICk7XG4iLCJ3aW5kb3cuRml4ZWRNZW51ID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCkge1xudmFyIGhlYWRyb29tID0gbmV3IEhlYWRyb29tKCk7XG4kKFwiI2hlYWRlclwiKS5oZWFkcm9vbSh7XG4gIFwib2Zmc2V0XCI6IDIwLFxuICBcInRvbGVyYW5jZVwiOiA1LFxuICBcImNsYXNzZXNcIjoge1xuICAgIFwiaW5pdGlhbFwiOiBcImFuaW1hdGVkXCIsXG4gICAgXCJwaW5uZWRcIjogXCJzbGlkZUluVXBcIixcbiAgICBcInVucGlubmVkXCI6IFwic2xpZGVJbkRvd25cIlxuICB9XG59KTtcblxuY29uc29sZS5sb2coaGVhZHJvb20pO1xufSAoIHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cuRml4ZWRNZW51ICkpXG4iLCIvKipcbiAqIEZpbGUgaGVyby1jYXJvdXNlbC5qc1xuICpcbiAqIENyZWF0ZSBhIGNhcm91c2VsIGlmIHdlIGhhdmUgbW9yZSB0aGFuIG9uZSBoZXJvIHNsaWRlLlxuICovXG53aW5kb3cud2RzSGVyb0Nhcm91c2VsID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKCB3aW5kb3cgKSxcblx0XHRcdGhlcm9DYXJvdXNlbDogJCggJy5jYXJvdXNlbCcgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy53aW5kb3cub24oICdsb2FkJywgYXBwLmRvU2xpY2sgKTtcblx0XHRhcHAuJGMud2luZG93Lm9uKCAnbG9hZCcsIGFwcC5kb0ZpcnN0QW5pbWF0aW9uICk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLmhlcm9DYXJvdXNlbC5sZW5ndGg7XG5cdH07XG5cblx0Ly8gQW5pbWF0ZSB0aGUgZmlyc3Qgc2xpZGUgb24gd2luZG93IGxvYWQuXG5cdGFwcC5kb0ZpcnN0QW5pbWF0aW9uID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBHZXQgdGhlIGZpcnN0IHNsaWRlIGNvbnRlbnQgYXJlYSBhbmQgYW5pbWF0aW9uIGF0dHJpYnV0ZS5cblx0XHRsZXQgZmlyc3RTbGlkZSA9IGFwcC4kYy5oZXJvQ2Fyb3VzZWwuZmluZCggJ1tkYXRhLXNsaWNrLWluZGV4PTBdJyApLFxuXHRcdFx0Zmlyc3RTbGlkZUNvbnRlbnQgPSBmaXJzdFNsaWRlLmZpbmQoICcuaGVyby1jb250ZW50JyApLFxuXHRcdFx0Zmlyc3RBbmltYXRpb24gPSBmaXJzdFNsaWRlQ29udGVudC5hdHRyKCAnZGF0YS1hbmltYXRpb24nICk7XG5cblx0XHQvLyBBZGQgdGhlIGFuaW1hdGlvbiBjbGFzcyB0byB0aGUgZmlyc3Qgc2xpZGUuXG5cdFx0Zmlyc3RTbGlkZUNvbnRlbnQuYWRkQ2xhc3MoIGZpcnN0QW5pbWF0aW9uICk7XG5cdH07XG5cblx0Ly8gQW5pbWF0ZSB0aGUgc2xpZGUgY29udGVudC5cblx0YXBwLmRvQW5pbWF0aW9uID0gZnVuY3Rpb24oKSB7XG5cdFx0bGV0IHNsaWRlcyA9ICQoICcuc2xpZGUnICksXG5cdFx0XHRhY3RpdmVTbGlkZSA9ICQoICcuc2xpY2stY3VycmVudCcgKSxcblx0XHRcdGFjdGl2ZUNvbnRlbnQgPSBhY3RpdmVTbGlkZS5maW5kKCAnLmhlcm8tY29udGVudCcgKSxcblxuXHRcdFx0Ly8gVGhpcyBpcyBhIHN0cmluZyBsaWtlIHNvOiAnYW5pbWF0ZWQgc29tZUNzc0NsYXNzJy5cblx0XHRcdGFuaW1hdGlvbkNsYXNzID0gYWN0aXZlQ29udGVudC5hdHRyKCAnZGF0YS1hbmltYXRpb24nICksXG5cdFx0XHRzcGxpdEFuaW1hdGlvbiA9IGFuaW1hdGlvbkNsYXNzLnNwbGl0KCAnICcgKSxcblxuXHRcdFx0Ly8gVGhpcyBpcyB0aGUgJ2FuaW1hdGVkJyBjbGFzcy5cblx0XHRcdGFuaW1hdGlvblRyaWdnZXIgPSBzcGxpdEFuaW1hdGlvblswXTtcblxuXHRcdC8vIEdvIHRocm91Z2ggZWFjaCBzbGlkZSB0byBzZWUgaWYgd2UndmUgYWxyZWFkeSBzZXQgYW5pbWF0aW9uIGNsYXNzZXMuXG5cdFx0c2xpZGVzLmVhY2goIGZ1bmN0aW9uKCkge1xuXHRcdFx0bGV0IHNsaWRlQ29udGVudCA9ICQoIHRoaXMgKS5maW5kKCAnLmhlcm8tY29udGVudCcgKTtcblxuXHRcdFx0Ly8gSWYgd2UndmUgc2V0IGFuaW1hdGlvbiBjbGFzc2VzIG9uIGEgc2xpZGUsIHJlbW92ZSB0aGVtLlxuXHRcdFx0aWYgKCBzbGlkZUNvbnRlbnQuaGFzQ2xhc3MoICdhbmltYXRlZCcgKSApIHtcblxuXHRcdFx0XHQvLyBHZXQgdGhlIGxhc3QgY2xhc3MsIHdoaWNoIGlzIHRoZSBhbmltYXRlLmNzcyBjbGFzcy5cblx0XHRcdFx0bGV0IGxhc3RDbGFzcyA9IHNsaWRlQ29udGVudFxuXHRcdFx0XHRcdC5hdHRyKCAnY2xhc3MnIClcblx0XHRcdFx0XHQuc3BsaXQoICcgJyApXG5cdFx0XHRcdFx0LnBvcCgpO1xuXG5cdFx0XHRcdC8vIFJlbW92ZSBib3RoIGFuaW1hdGlvbiBjbGFzc2VzLlxuXHRcdFx0XHRzbGlkZUNvbnRlbnQucmVtb3ZlQ2xhc3MoIGxhc3RDbGFzcyApLnJlbW92ZUNsYXNzKCBhbmltYXRpb25UcmlnZ2VyICk7XG5cdFx0XHR9XG5cdFx0fSApO1xuXG5cdFx0Ly8gQWRkIGFuaW1hdGlvbiBjbGFzc2VzIGFmdGVyIHNsaWRlIGlzIGluIHZpZXcuXG5cdFx0YWN0aXZlQ29udGVudC5hZGRDbGFzcyggYW5pbWF0aW9uQ2xhc3MgKTtcblx0fTtcblxuXHQvLyBBbGxvdyBiYWNrZ3JvdW5kIHZpZGVvcyB0byBhdXRvcGxheS5cblx0YXBwLnBsYXlCYWNrZ3JvdW5kVmlkZW9zID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBHZXQgYWxsIHRoZSB2aWRlb3MgaW4gb3VyIHNsaWRlcyBvYmplY3QuXG5cdFx0JCggJ3ZpZGVvJyApLmVhY2goIGZ1bmN0aW9uKCkge1xuXG5cdFx0XHQvLyBMZXQgdGhlbSBhdXRvcGxheS4gVE9ETzogUG9zc2libHkgY2hhbmdlIHRoaXMgbGF0ZXIgdG8gb25seSBwbGF5IHRoZSB2aXNpYmxlIHNsaWRlIHZpZGVvLlxuXHRcdFx0dGhpcy5wbGF5KCk7XG5cdFx0fSApO1xuXHR9O1xuXG5cdC8vIEtpY2sgb2ZmIFNsaWNrLlxuXHRhcHAuZG9TbGljayA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy5oZXJvQ2Fyb3VzZWwub24oICdpbml0JywgYXBwLnBsYXlCYWNrZ3JvdW5kVmlkZW9zICk7XG5cblx0XHRhcHAuJGMuaGVyb0Nhcm91c2VsLnNsaWNrKCB7XG5cdFx0XHRhdXRvcGxheTogdHJ1ZSxcblx0XHRcdGF1dG9wbGF5U3BlZWQ6IDUwMDAsXG5cdFx0XHRhcnJvd3M6IGZhbHNlLFxuXHRcdFx0ZG90czogZmFsc2UsXG5cdFx0XHRmb2N1c09uU2VsZWN0OiB0cnVlLFxuXHRcdFx0d2FpdEZvckFuaW1hdGU6IHRydWVcblx0XHR9ICk7XG5cblx0XHRhcHAuJGMuaGVyb0Nhcm91c2VsLm9uKCAnYWZ0ZXJDaGFuZ2UnLCBhcHAuZG9BbmltYXRpb24gKTtcblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNIZXJvQ2Fyb3VzZWwgKSApO1xuIiwiLyoqXG4gKiBGaWxlIGpzLWVuYWJsZWQuanNcbiAqXG4gKiBJZiBKYXZhc2NyaXB0IGlzIGVuYWJsZWQsIHJlcGxhY2UgdGhlIDxib2R5PiBjbGFzcyBcIm5vLWpzXCIuXG4gKi9cbmRvY3VtZW50LmJvZHkuY2xhc3NOYW1lID0gZG9jdW1lbnQuYm9keS5jbGFzc05hbWUucmVwbGFjZSggJ25vLWpzJywgJ2pzJyApO1xuIiwiY29uc3Qgc29tZXZhciA9IFwic29tZXZhclwiO1xuY29uc29sZS5sb2coc29tZXZhcik7XG53aW5kb3cuTWFzb25yeUdhbGxlcnkgPSB7fTtcbiggZnVuY3Rpb24oIHdpbmRvdywgJCwgYXBwLCBNYXNvbnJ5KSB7XG4gIC8vIENvbnN0cnVjdG9yLlxuICBhcHAuaW5pdCA9IGZ1bmN0aW9uKCkge1xuICAgIGFwcC5jYWNoZVNvbWUoKTtcbiAgfVxuICBhcHAuY2FjaGVTb21lID0gZnVuY3Rpb24oKXtcbiAgICBjb25zb2xlLmxvZyhcImNhY2hlZFwiKTtcbiAgfVxuLy8gdmFyIGVsZW0gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuZ2FsbGVyeS1hcnRpc3Qtc2loYScpO1xuLy8gdmFyIG1zbnJ5ID0gbmV3IE1hc29ucnkoIGVsZW0sIHtcbi8vICAgLy8gb3B0aW9uc1xuLy8gICBpdGVtU2VsZWN0b3I6ICcuZ2FsbGVyeS1pdGVtLXNhbmRib3gnLFxuLy8gICBjb2x1bW5XaWR0aDogMjAwLFxuLy8gICBwZXJjZW50UG9zaXRpb246IHRydWVcbi8vIH0pO1xuLy8gY29uc29sZS5sb2coZWxlbSk7XHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59ICggd2luZG93LCBqUXVlcnksIHdpbmRvdy5NYXNvbnJ5R2FsbGVyeSwgTWFzb25yeSkpO1xuIiwiLyoqXG4gKiBGaWxlOiBtb2JpbGUtbWVudS5qc1xuICpcbiAqIENyZWF0ZSBhbiBhY2NvcmRpb24gc3R5bGUgZHJvcGRvd24uXG4gKi9cbndpbmRvdy53ZHNNb2JpbGVNZW51ID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0Ym9keTogJCggJ2JvZHknICksXG5cdFx0XHR3aW5kb3c6ICQoIHdpbmRvdyApLFxuXHRcdFx0c3ViTWVudUNvbnRhaW5lcjogJCggJy5tb2JpbGUtbWVudSAuc3ViLW1lbnUsIC51dGlsaXR5LW5hdmlnYXRpb24gLnN1Yi1tZW51JyApLFxuXHRcdFx0c3ViU3ViTWVudUNvbnRhaW5lcjogJCggJy5tb2JpbGUtbWVudSAuc3ViLW1lbnUgLnN1Yi1tZW51JyApLFxuXHRcdFx0c3ViTWVudVBhcmVudEl0ZW06ICQoICcubW9iaWxlLW1lbnUgbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbiwgLnV0aWxpdHktbmF2aWdhdGlvbiBsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApLFxuXHRcdFx0b2ZmQ2FudmFzQ29udGFpbmVyOiAkKCAnLm9mZi1jYW52YXMtY29udGFpbmVyJyApXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLndpbmRvdy5vbiggJ2xvYWQnLCBhcHAuYWRkRG93bkFycm93ICk7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLm9uKCAnY2xpY2snLCBhcHAudG9nZ2xlU3VibWVudSApO1xuXHRcdGFwcC4kYy5zdWJNZW51UGFyZW50SXRlbS5vbiggJ3RyYW5zaXRpb25lbmQnLCBhcHAucmVzZXRTdWJNZW51ICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5vbiggJ3RyYW5zaXRpb25lbmQnLCBhcHAuZm9yY2VDbG9zZVN1Ym1lbnVzICk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLnN1Yk1lbnVDb250YWluZXIubGVuZ3RoO1xuXHR9O1xuXG5cdC8vIFJlc2V0IHRoZSBzdWJtZW51cyBhZnRlciBpdCdzIGRvbmUgY2xvc2luZy5cblx0YXBwLnJlc2V0U3ViTWVudSA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0Ly8gV2hlbiB0aGUgbGlzdCBpdGVtIGlzIGRvbmUgdHJhbnNpdGlvbmluZyBpbiBoZWlnaHQsXG5cdFx0Ly8gcmVtb3ZlIHRoZSBjbGFzc2VzIGZyb20gdGhlIHN1Ym1lbnUgc28gaXQgaXMgcmVhZHkgdG8gdG9nZ2xlIGFnYWluLlxuXHRcdGlmICggJCggdGhpcyApLmlzKCAnbGkubWVudS1pdGVtLWhhcy1jaGlsZHJlbicgKSAmJiAhICQoIHRoaXMgKS5oYXNDbGFzcyggJ2lzLXZpc2libGUnICkgKSB7XG5cdFx0XHQkKCB0aGlzICkuZmluZCggJ3VsLnN1Yi1tZW51JyApLnJlbW92ZUNsYXNzKCAnc2xpZGVPdXRMZWZ0IGlzLXZpc2libGUnICk7XG5cdFx0fVxuXG5cdH07XG5cblx0Ly8gU2xpZGUgb3V0IHRoZSBzdWJtZW51IGl0ZW1zLlxuXHRhcHAuc2xpZGVPdXRTdWJNZW51cyA9IGZ1bmN0aW9uKCBlbCApIHtcblxuXHRcdC8vIElmIHRoaXMgaXRlbSdzIHBhcmVudCBpcyB2aXNpYmxlIGFuZCB0aGlzIGlzIG5vdCwgYmFpbC5cblx0XHRpZiAoIGVsLnBhcmVudCgpLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSAmJiAhIGVsLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSApIHtcblx0XHRcdHJldHVybjtcblx0XHR9XG5cblx0XHQvLyBJZiB0aGlzIGl0ZW0ncyBwYXJlbnQgaXMgdmlzaWJsZSBhbmQgdGhpcyBpdGVtIGlzIHZpc2libGUsIGhpZGUgaXRzIHN1Ym1lbnUgdGhlbiBiYWlsLlxuXHRcdGlmICggZWwucGFyZW50KCkuaGFzQ2xhc3MoICdpcy12aXNpYmxlJyApICYmIGVsLmhhc0NsYXNzKCAnaXMtdmlzaWJsZScgKSApIHtcblx0XHRcdGVsLnJlbW92ZUNsYXNzKCAnaXMtdmlzaWJsZScgKS5maW5kKCAnLnN1Yi1tZW51JyApLnJlbW92ZUNsYXNzKCAnc2xpZGVJbkxlZnQnICkuYWRkQ2xhc3MoICdzbGlkZU91dExlZnQnICk7XG5cdFx0XHRyZXR1cm47XG5cdFx0fVxuXG5cdFx0YXBwLiRjLnN1Yk1lbnVDb250YWluZXIuZWFjaCggZnVuY3Rpb24oKSB7XG5cblx0XHRcdC8vIE9ubHkgdHJ5IHRvIGNsb3NlIHN1Ym1lbnVzIHRoYXQgYXJlIGFjdHVhbGx5IG9wZW4uXG5cdFx0XHRpZiAoICQoIHRoaXMgKS5oYXNDbGFzcyggJ3NsaWRlSW5MZWZ0JyApICkge1xuXG5cdFx0XHRcdC8vIENsb3NlIHRoZSBwYXJlbnQgbGlzdCBpdGVtLCBhbmQgc2V0IHRoZSBjb3JyZXNwb25kaW5nIGJ1dHRvbiBhcmlhIHRvIGZhbHNlLlxuXHRcdFx0XHQkKCB0aGlzICkucGFyZW50KCkucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApLmZpbmQoICcucGFyZW50LWluZGljYXRvcicgKS5hdHRyKCAnYXJpYS1leHBhbmRlZCcsIGZhbHNlICk7XG5cblx0XHRcdFx0Ly8gU2xpZGUgb3V0IHRoZSBzdWJtZW51LlxuXHRcdFx0XHQkKCB0aGlzICkucmVtb3ZlQ2xhc3MoICdzbGlkZUluTGVmdCcgKS5hZGRDbGFzcyggJ3NsaWRlT3V0TGVmdCcgKTtcblx0XHRcdH1cblxuXHRcdH0gKTtcblx0fTtcblxuXHQvLyBBZGQgdGhlIGRvd24gYXJyb3cgdG8gc3VibWVudSBwYXJlbnRzLlxuXHRhcHAuYWRkRG93bkFycm93ID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLnByZXBlbmQoICc8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBhcmlhLWV4cGFuZGVkPVwiZmFsc2VcIiBjbGFzcz1cInBhcmVudC1pbmRpY2F0b3JcIiBhcmlhLWxhYmVsPVwiT3BlbiBzdWJtZW51XCI+PHNwYW4gY2xhc3M9XCJkb3duLWFycm93XCI+PC9zcGFuPjwvYnV0dG9uPicgKTtcblx0fTtcblxuXHQvLyBEZWFsIHdpdGggdGhlIHN1Ym1lbnUuXG5cdGFwcC50b2dnbGVTdWJtZW51ID0gZnVuY3Rpb24oIGUgKSB7XG5cblx0XHRsZXQgZWwgPSAkKCB0aGlzICksIC8vIFRoZSBtZW51IGVsZW1lbnQgd2hpY2ggd2FzIGNsaWNrZWQgb24uXG5cdFx0XHRzdWJNZW51ID0gZWwuY2hpbGRyZW4oICd1bC5zdWItbWVudScgKSwgLy8gVGhlIG5lYXJlc3Qgc3VibWVudS5cblx0XHRcdCR0YXJnZXQgPSAkKCBlLnRhcmdldCApOyAvLyB0aGUgZWxlbWVudCB0aGF0J3MgYWN0dWFsbHkgYmVpbmcgY2xpY2tlZCAoY2hpbGQgb2YgdGhlIGxpIHRoYXQgdHJpZ2dlcmVkIHRoZSBjbGljayBldmVudCkuXG5cblx0XHQvLyBGaWd1cmUgb3V0IGlmIHdlJ3JlIGNsaWNraW5nIHRoZSBidXR0b24gb3IgaXRzIGFycm93IGNoaWxkLFxuXHRcdC8vIGlmIHNvLCB3ZSBjYW4ganVzdCBvcGVuIG9yIGNsb3NlIHRoZSBtZW51IGFuZCBiYWlsLlxuXHRcdGlmICggJHRhcmdldC5oYXNDbGFzcyggJ2Rvd24tYXJyb3cnICkgfHwgJHRhcmdldC5oYXNDbGFzcyggJ3BhcmVudC1pbmRpY2F0b3InICkgKSB7XG5cblx0XHRcdC8vIEZpcnN0LCBjb2xsYXBzZSBhbnkgYWxyZWFkeSBvcGVuZWQgc3VibWVudXMuXG5cdFx0XHRhcHAuc2xpZGVPdXRTdWJNZW51cyggZWwgKTtcblxuXHRcdFx0aWYgKCAhIHN1Yk1lbnUuaGFzQ2xhc3MoICdpcy12aXNpYmxlJyApICkge1xuXG5cdFx0XHRcdC8vIE9wZW4gdGhlIHN1Ym1lbnUuXG5cdFx0XHRcdGFwcC5vcGVuU3VibWVudSggZWwsIHN1Yk1lbnUgKTtcblxuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0fVxuXG5cdH07XG5cblx0Ly8gT3BlbiBhIHN1Ym1lbnUuXG5cdGFwcC5vcGVuU3VibWVudSA9IGZ1bmN0aW9uKCBwYXJlbnQsIHN1Yk1lbnUgKSB7XG5cblx0XHQvLyBFeHBhbmQgdGhlIGxpc3QgbWVudSBpdGVtLCBhbmQgc2V0IHRoZSBjb3JyZXNwb25kaW5nIGJ1dHRvbiBhcmlhIHRvIHRydWUuXG5cdFx0cGFyZW50LmFkZENsYXNzKCAnaXMtdmlzaWJsZScgKS5maW5kKCAnLnBhcmVudC1pbmRpY2F0b3InICkuYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCB0cnVlICk7XG5cblx0XHQvLyBTbGlkZSB0aGUgbWVudSBpbi5cblx0XHRzdWJNZW51LmFkZENsYXNzKCAnaXMtdmlzaWJsZSBhbmltYXRlZCBzbGlkZUluTGVmdCcgKTtcblx0fTtcblxuXHQvLyBGb3JjZSBjbG9zZSBhbGwgdGhlIHN1Ym1lbnVzIHdoZW4gdGhlIG1haW4gbWVudSBjb250YWluZXIgaXMgY2xvc2VkLlxuXHRhcHAuZm9yY2VDbG9zZVN1Ym1lbnVzID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBUaGUgdHJhbnNpdGlvbmVuZCBldmVudCB0cmlnZ2VycyBvbiBvcGVuIGFuZCBvbiBjbG9zZSwgbmVlZCB0byBtYWtlIHN1cmUgd2Ugb25seSBkbyB0aGlzIG9uIGNsb3NlLlxuXHRcdGlmICggISAkKCB0aGlzICkuaGFzQ2xhc3MoICdpcy12aXNpYmxlJyApICkge1xuXHRcdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLnJlbW92ZUNsYXNzKCAnaXMtdmlzaWJsZScgKS5maW5kKCAnLnBhcmVudC1pbmRpY2F0b3InICkuYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCBmYWxzZSApO1xuXHRcdFx0YXBwLiRjLnN1Yk1lbnVDb250YWluZXIucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlIHNsaWRlSW5MZWZ0JyApO1xuXHRcdFx0YXBwLiRjLmJvZHkuY3NzKCAnb3ZlcmZsb3cnLCAndmlzaWJsZScgKTtcblx0XHRcdGFwcC4kYy5ib2R5LnVuYmluZCggJ3RvdWNoc3RhcnQnICk7XG5cdFx0fVxuXG5cdFx0aWYgKCAkKCB0aGlzICkuaGFzQ2xhc3MoICdpcy12aXNpYmxlJyApICkge1xuXHRcdFx0YXBwLiRjLmJvZHkuY3NzKCAnb3ZlcmZsb3cnLCAnaGlkZGVuJyApO1xuXHRcdFx0YXBwLiRjLmJvZHkuYmluZCggJ3RvdWNoc3RhcnQnLCBmdW5jdGlvbiggZSApIHtcblx0XHRcdFx0aWYgKCAhICQoIGUudGFyZ2V0ICkucGFyZW50cyggJy5jb250YWN0LW1vZGFsJyApWzBdICkge1xuXHRcdFx0XHRcdGUucHJldmVudERlZmF1bHQoKTtcblx0XHRcdFx0fVxuXHRcdFx0fSApO1xuXHRcdH1cblx0fTtcblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG5cbn0oIHdpbmRvdywgalF1ZXJ5LCB3aW5kb3cud2RzTW9iaWxlTWVudSApICk7XG4iLCIvKipcbiAqIEZpbGUgbW9kYWwuanNcbiAqXG4gKiBEZWFsIHdpdGggbXVsdGlwbGUgbW9kYWxzIGFuZCB0aGVpciBtZWRpYS5cbiAqL1xud2luZG93Lndkc01vZGFsID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHRsZXQgJG1vZGFsVG9nZ2xlLFxuXHRcdCRmb2N1c2FibGVDaGlsZHJlbixcblx0XHQkcGxheWVyLFxuXHRcdCR0YWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCAnc2NyaXB0JyApLFxuXHRcdCRmaXJzdFNjcmlwdFRhZyA9IGRvY3VtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCAnc2NyaXB0JyApWzBdLFxuXHRcdFlUO1xuXG5cdC8vIENvbnN0cnVjdG9yLlxuXHRhcHAuaW5pdCA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC5jYWNoZSgpO1xuXG5cdFx0aWYgKCBhcHAubWVldHNSZXF1aXJlbWVudHMoKSApIHtcblx0XHRcdCRmaXJzdFNjcmlwdFRhZy5wYXJlbnROb2RlLmluc2VydEJlZm9yZSggJHRhZywgJGZpcnN0U2NyaXB0VGFnICk7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0J2JvZHknOiAkKCAnYm9keScgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gJCggJy5tb2RhbC10cmlnZ2VyJyApLmxlbmd0aDtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBUcmlnZ2VyIGEgbW9kYWwgdG8gb3Blbi5cblx0XHRhcHAuJGMuYm9keS5vbiggJ2NsaWNrIHRvdWNoc3RhcnQnLCAnLm1vZGFsLXRyaWdnZXInLCBhcHAub3Blbk1vZGFsICk7XG5cblx0XHQvLyBUcmlnZ2VyIHRoZSBjbG9zZSBidXR0b24gdG8gY2xvc2UgdGhlIG1vZGFsLlxuXHRcdGFwcC4kYy5ib2R5Lm9uKCAnY2xpY2sgdG91Y2hzdGFydCcsICcuY2xvc2UnLCBhcHAuY2xvc2VNb2RhbCApO1xuXG5cdFx0Ly8gQWxsb3cgdGhlIHVzZXIgdG8gY2xvc2UgdGhlIG1vZGFsIGJ5IGhpdHRpbmcgdGhlIGVzYyBrZXkuXG5cdFx0YXBwLiRjLmJvZHkub24oICdrZXlkb3duJywgYXBwLmVzY0tleUNsb3NlICk7XG5cblx0XHQvLyBBbGxvdyB0aGUgdXNlciB0byBjbG9zZSB0aGUgbW9kYWwgYnkgY2xpY2tpbmcgb3V0c2lkZSBvZiB0aGUgbW9kYWwuXG5cdFx0YXBwLiRjLmJvZHkub24oICdjbGljayB0b3VjaHN0YXJ0JywgJ2Rpdi5tb2RhbC1vcGVuJywgYXBwLmNsb3NlTW9kYWxCeUNsaWNrICk7XG5cblx0XHQvLyBMaXN0ZW4gdG8gdGFicywgdHJhcCBrZXlib2FyZCBpZiB3ZSBuZWVkIHRvXG5cdFx0YXBwLiRjLmJvZHkub24oICdrZXlkb3duJywgYXBwLnRyYXBLZXlib2FyZE1heWJlICk7XG5cblx0fTtcblxuXHQvLyBPcGVuIHRoZSBtb2RhbC5cblx0YXBwLm9wZW5Nb2RhbCA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0Ly8gU3RvcmUgdGhlIG1vZGFsIHRvZ2dsZSBlbGVtZW50XG5cdFx0JG1vZGFsVG9nZ2xlID0gJCggdGhpcyApO1xuXG5cdFx0Ly8gRmlndXJlIG91dCB3aGljaCBtb2RhbCB3ZSdyZSBvcGVuaW5nIGFuZCBzdG9yZSB0aGUgb2JqZWN0LlxuXHRcdGxldCAkbW9kYWwgPSAkKCAkKCB0aGlzICkuZGF0YSggJ3RhcmdldCcgKSApO1xuXG5cdFx0Ly8gRGlzcGxheSB0aGUgbW9kYWwuXG5cdFx0JG1vZGFsLmFkZENsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIEFkZCBib2R5IGNsYXNzLlxuXHRcdGFwcC4kYy5ib2R5LmFkZENsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIEZpbmQgdGhlIGZvY3VzYWJsZSBjaGlsZHJlbiBvZiB0aGUgbW9kYWwuXG5cdFx0Ly8gVGhpcyBsaXN0IG1heSBiZSBpbmNvbXBsZXRlLCByZWFsbHkgd2lzaCBqUXVlcnkgaGFkIHRoZSA6Zm9jdXNhYmxlIHBzZXVkbyBsaWtlIGpRdWVyeSBVSSBkb2VzLlxuXHRcdC8vIEZvciBtb3JlIGFib3V0IDppbnB1dCBzZWU6IGh0dHBzOi8vYXBpLmpxdWVyeS5jb20vaW5wdXQtc2VsZWN0b3IvXG5cdFx0JGZvY3VzYWJsZUNoaWxkcmVuID0gJG1vZGFsLmZpbmQoICdhLCA6aW5wdXQsIFt0YWJpbmRleF0nICk7XG5cblx0XHQvLyBJZGVhbGx5LCB0aGVyZSBpcyBhbHdheXMgb25lICh0aGUgY2xvc2UgYnV0dG9uKSwgYnV0IHlvdSBuZXZlciBrbm93LlxuXHRcdGlmICggMCA8ICRmb2N1c2FibGVDaGlsZHJlbi5sZW5ndGggKSB7XG5cblx0XHRcdC8vIFNoaWZ0IGZvY3VzIHRvIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudC5cblx0XHRcdCRmb2N1c2FibGVDaGlsZHJlblswXS5mb2N1cygpO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIENsb3NlIHRoZSBtb2RhbC5cblx0YXBwLmNsb3NlTW9kYWwgPSBmdW5jdGlvbigpIHtcblxuXHRcdC8vIEZpZ3VyZSB0aGUgb3BlbmVkIG1vZGFsIHdlJ3JlIGNsb3NpbmcgYW5kIHN0b3JlIHRoZSBvYmplY3QuXG5cdFx0bGV0ICRtb2RhbCA9ICQoICQoICdkaXYubW9kYWwtb3BlbiAuY2xvc2UnICkuZGF0YSggJ3RhcmdldCcgKSApLFxuXG5cdFx0XHQvLyBGaW5kIHRoZSBpZnJhbWUgaW4gdGhlICRtb2RhbCBvYmplY3QuXG5cdFx0XHQkaWZyYW1lID0gJG1vZGFsLmZpbmQoICdpZnJhbWUnICk7XG5cblx0XHQvLyBPbmx5IGRvIHRoaXMgaWYgdGhlcmUgYXJlIGFueSBpZnJhbWVzLlxuXHRcdGlmICggJGlmcmFtZS5sZW5ndGggKSB7XG5cblx0XHRcdC8vIEdldCB0aGUgaWZyYW1lIHNyYyBVUkwuXG5cdFx0XHRsZXQgdXJsID0gJGlmcmFtZS5hdHRyKCAnc3JjJyApO1xuXG5cdFx0XHQvLyBSZW1vdmluZy9SZWFkZGluZyB0aGUgVVJMIHdpbGwgZWZmZWN0aXZlbHkgYnJlYWsgdGhlIFlvdVR1YmUgQVBJLlxuXHRcdFx0Ly8gU28gbGV0J3Mgbm90IGRvIHRoYXQgd2hlbiB0aGUgaWZyYW1lIFVSTCBjb250YWlucyB0aGUgZW5hYmxlanNhcGkgcGFyYW1ldGVyLlxuXHRcdFx0aWYgKCAhIHVybC5pbmNsdWRlcyggJ2VuYWJsZWpzYXBpPTEnICkgKSB7XG5cblx0XHRcdFx0Ly8gUmVtb3ZlIHRoZSBzb3VyY2UgVVJMLCB0aGVuIGFkZCBpdCBiYWNrLCBzbyB0aGUgdmlkZW8gY2FuIGJlIHBsYXllZCBhZ2FpbiBsYXRlci5cblx0XHRcdFx0JGlmcmFtZS5hdHRyKCAnc3JjJywgJycgKS5hdHRyKCAnc3JjJywgdXJsICk7XG5cdFx0XHR9IGVsc2Uge1xuXG5cdFx0XHRcdC8vIFVzZSB0aGUgWW91VHViZSBBUEkgdG8gc3RvcCB0aGUgdmlkZW8uXG5cdFx0XHRcdCRwbGF5ZXIuc3RvcFZpZGVvKCk7XG5cdFx0XHR9XG5cdFx0fVxuXG5cdFx0Ly8gRmluYWxseSwgaGlkZSB0aGUgbW9kYWwuXG5cdFx0JG1vZGFsLnJlbW92ZUNsYXNzKCAnbW9kYWwtb3BlbicgKTtcblxuXHRcdC8vIFJlbW92ZSB0aGUgYm9keSBjbGFzcy5cblx0XHRhcHAuJGMuYm9keS5yZW1vdmVDbGFzcyggJ21vZGFsLW9wZW4nICk7XG5cblx0XHQvLyBSZXZlcnQgZm9jdXMgYmFjayB0byB0b2dnbGUgZWxlbWVudFxuXHRcdCRtb2RhbFRvZ2dsZS5mb2N1cygpO1xuXG5cdH07XG5cblx0Ly8gQ2xvc2UgaWYgXCJlc2NcIiBrZXkgaXMgcHJlc3NlZC5cblx0YXBwLmVzY0tleUNsb3NlID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdGlmICggMjcgPT09IGV2ZW50LmtleUNvZGUgKSB7XG5cdFx0XHRhcHAuY2xvc2VNb2RhbCgpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDbG9zZSBpZiB0aGUgdXNlciBjbGlja3Mgb3V0c2lkZSBvZiB0aGUgbW9kYWxcblx0YXBwLmNsb3NlTW9kYWxCeUNsaWNrID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXG5cdFx0Ly8gSWYgdGhlIHBhcmVudCBjb250YWluZXIgaXMgTk9UIHRoZSBtb2RhbCBkaWFsb2cgY29udGFpbmVyLCBjbG9zZSB0aGUgbW9kYWxcblx0XHRpZiAoICEgJCggZXZlbnQudGFyZ2V0ICkucGFyZW50cyggJ2RpdicgKS5oYXNDbGFzcyggJ21vZGFsLWRpYWxvZycgKSApIHtcblx0XHRcdGFwcC5jbG9zZU1vZGFsKCk7XG5cdFx0fVxuXHR9O1xuXG5cdC8vIFRyYXAgdGhlIGtleWJvYXJkIGludG8gYSBtb2RhbCB3aGVuIG9uZSBpcyBhY3RpdmUuXG5cdGFwcC50cmFwS2V5Ym9hcmRNYXliZSA9IGZ1bmN0aW9uKCBldmVudCApIHtcblxuXHRcdC8vIFdlIG9ubHkgbmVlZCB0byBkbyBzdHVmZiB3aGVuIHRoZSBtb2RhbCBpcyBvcGVuIGFuZCB0YWIgaXMgcHJlc3NlZC5cblx0XHRpZiAoIDkgPT09IGV2ZW50LndoaWNoICYmIDAgPCAkKCAnLm1vZGFsLW9wZW4nICkubGVuZ3RoICkge1xuXHRcdFx0bGV0ICRmb2N1c2VkID0gJCggJzpmb2N1cycgKSxcblx0XHRcdFx0Zm9jdXNJbmRleCA9ICRmb2N1c2FibGVDaGlsZHJlbi5pbmRleCggJGZvY3VzZWQgKTtcblxuXHRcdFx0aWYgKCAwID09PSBmb2N1c0luZGV4ICYmIGV2ZW50LnNoaWZ0S2V5ICkge1xuXG5cdFx0XHRcdC8vIElmIHRoaXMgaXMgdGhlIGZpcnN0IGZvY3VzYWJsZSBlbGVtZW50LCBhbmQgc2hpZnQgaXMgaGVsZCB3aGVuIHByZXNzaW5nIHRhYiwgZ28gYmFjayB0byBsYXN0IGZvY3VzYWJsZSBlbGVtZW50LlxuXHRcdFx0XHQkZm9jdXNhYmxlQ2hpbGRyZW5bICRmb2N1c2FibGVDaGlsZHJlbi5sZW5ndGggLSAxIF0uZm9jdXMoKTtcblx0XHRcdFx0ZXZlbnQucHJldmVudERlZmF1bHQoKTtcblx0XHRcdH0gZWxzZSBpZiAoICEgZXZlbnQuc2hpZnRLZXkgJiYgZm9jdXNJbmRleCA9PT0gJGZvY3VzYWJsZUNoaWxkcmVuLmxlbmd0aCAtIDEgKSB7XG5cblx0XHRcdFx0Ly8gSWYgdGhpcyBpcyB0aGUgbGFzdCBmb2N1c2FibGUgZWxlbWVudCwgYW5kIHNoaWZ0IGlzIG5vdCBoZWxkLCBnbyBiYWNrIHRvIHRoZSBmaXJzdCBmb2N1c2FibGUgZWxlbWVudC5cblx0XHRcdFx0JGZvY3VzYWJsZUNoaWxkcmVuWzBdLmZvY3VzKCk7XG5cdFx0XHRcdGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9O1xuXG5cdC8vIEhvb2sgaW50byBZb3VUdWJlIDxpZnJhbWU+LlxuXHRhcHAub25Zb3VUdWJlSWZyYW1lQVBJUmVhZHkgPSBmdW5jdGlvbigpIHtcblx0XHRsZXQgJG1vZGFsID0gJCggJ2Rpdi5tb2RhbCcgKSxcblx0XHRcdCRpZnJhbWVpZCA9ICRtb2RhbC5maW5kKCAnaWZyYW1lJyApLmF0dHIoICdpZCcgKTtcblxuXHRcdCRwbGF5ZXIgPSBuZXcgWVQuUGxheWVyKCAkaWZyYW1laWQsIHtcblx0XHRcdGV2ZW50czoge1xuXHRcdFx0XHQnb25SZWFkeSc6IGFwcC5vblBsYXllclJlYWR5LFxuXHRcdFx0XHQnb25TdGF0ZUNoYW5nZSc6IGFwcC5vblBsYXllclN0YXRlQ2hhbmdlXG5cdFx0XHR9XG5cdFx0fSApO1xuXHR9O1xuXG5cdC8vIERvIHNvbWV0aGluZyBvbiBwbGF5ZXIgcmVhZHkuXG5cdGFwcC5vblBsYXllclJlYWR5ID0gZnVuY3Rpb24oKSB7XG5cdH07XG5cblx0Ly8gRG8gc29tZXRoaW5nIG9uIHBsYXllciBzdGF0ZSBjaGFuZ2UuXG5cdGFwcC5vblBsYXllclN0YXRlQ2hhbmdlID0gZnVuY3Rpb24oKSB7XG5cblx0XHQvLyBTZXQgZm9jdXMgdG8gdGhlIGZpcnN0IGZvY3VzYWJsZSBlbGVtZW50IGluc2lkZSBvZiB0aGUgbW9kYWwgdGhlIHBsYXllciBpcyBpbi5cblx0XHQkKCBldmVudC50YXJnZXQuYSApLnBhcmVudHMoICcubW9kYWwnICkuZmluZCggJ2EsIDppbnB1dCwgW3RhYmluZGV4XScgKS5maXJzdCgpLmZvY3VzKCk7XG5cdH07XG5cblxuXHQvLyBFbmdhZ2UhXG5cdCQoIGFwcC5pbml0ICk7XG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc01vZGFsICkgKTtcbiIsIi8qKlxuICogRmlsZTogbmF2aWdhdGlvbi1wcmltYXJ5LmpzXG4gKlxuICogSGVscGVycyBmb3IgdGhlIHByaW1hcnkgbmF2aWdhdGlvbi5cbiAqL1xud2luZG93Lndkc1ByaW1hcnlOYXZpZ2F0aW9uID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0d2luZG93OiAkKCB3aW5kb3cgKSxcblx0XHRcdHN1Yk1lbnVDb250YWluZXI6ICQoICcubWFpbi1uYXZpZ2F0aW9uIC5zdWItbWVudScgKSxcblx0XHRcdHN1Yk1lbnVQYXJlbnRJdGVtOiAkKCAnLm1haW4tbmF2aWdhdGlvbiBsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApXG5cdFx0fTtcblx0fTtcblxuXHQvLyBDb21iaW5lIGFsbCBldmVudHMuXG5cdGFwcC5iaW5kRXZlbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLndpbmRvdy5vbiggJ2xvYWQnLCBhcHAuYWRkRG93bkFycm93ICk7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLmZpbmQoICdhJyApLm9uKCAnZm9jdXNpbiBmb2N1c291dCcsIGFwcC50b2dnbGVGb2N1cyApO1xuXHR9O1xuXG5cdC8vIERvIHdlIG1lZXQgdGhlIHJlcXVpcmVtZW50cz9cblx0YXBwLm1lZXRzUmVxdWlyZW1lbnRzID0gZnVuY3Rpb24oKSB7XG5cdFx0cmV0dXJuIGFwcC4kYy5zdWJNZW51Q29udGFpbmVyLmxlbmd0aDtcblx0fTtcblxuXHQvLyBBZGQgdGhlIGRvd24gYXJyb3cgdG8gc3VibWVudSBwYXJlbnRzLlxuXHRhcHAuYWRkRG93bkFycm93ID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLnN1Yk1lbnVQYXJlbnRJdGVtLmZpbmQoICc+IGEnICkuYXBwZW5kKCAnPHNwYW4gY2xhc3M9XCJjYXJldC1kb3duXCIgYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9zcGFuPicgKTtcblx0fTtcblxuXHQvLyBUb2dnbGUgdGhlIGZvY3VzIGNsYXNzIG9uIHRoZSBsaW5rIHBhcmVudC5cblx0YXBwLnRvZ2dsZUZvY3VzID0gZnVuY3Rpb24oKSB7XG5cdFx0JCggdGhpcyApLnBhcmVudHMoICdsaS5tZW51LWl0ZW0taGFzLWNoaWxkcmVuJyApLnRvZ2dsZUNsYXNzKCAnZm9jdXMnICk7XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc1ByaW1hcnlOYXZpZ2F0aW9uICkgKTtcbiIsIi8qKlxuICogRmlsZTogb2ZmLWNhbnZhcy5qc1xuICpcbiAqIEhlbHAgZGVhbCB3aXRoIHRoZSBvZmYtY2FudmFzIG1vYmlsZSBtZW51LlxuICovXG53aW5kb3cud2Rzb2ZmQ2FudmFzID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblxuXHRcdGlmICggYXBwLm1lZXRzUmVxdWlyZW1lbnRzKCkgKSB7XG5cdFx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHRcdH1cblx0fTtcblxuXHQvLyBDYWNoZSBhbGwgdGhlIHRoaW5ncy5cblx0YXBwLmNhY2hlID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjID0ge1xuXHRcdFx0Ym9keTogJCggJ2JvZHknICksXG5cdFx0XHRvZmZDYW52YXNDbG9zZTogJCggJy5vZmYtY2FudmFzLWNsb3NlJyApLFxuXHRcdFx0b2ZmQ2FudmFzQ29udGFpbmVyOiAkKCAnLm9mZi1jYW52YXMtY29udGFpbmVyJyApLFxuXHRcdFx0b2ZmQ2FudmFzT3BlbjogJCggJy5vZmYtY2FudmFzLW9wZW4nICksXG5cdFx0XHRvZmZDYW52YXNTY3JlZW46ICQoICcub2ZmLWNhbnZhcy1zY3JlZW4nIClcblx0XHR9O1xuXHR9O1xuXG5cdC8vIENvbWJpbmUgYWxsIGV2ZW50cy5cblx0YXBwLmJpbmRFdmVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMuYm9keS5vbiggJ2tleWRvd24nLCBhcHAuZXNjS2V5Q2xvc2UgKTtcblx0XHRhcHAuJGMub2ZmQ2FudmFzQ2xvc2Uub24oICdjbGljaycsIGFwcC5jbG9zZW9mZkNhbnZhcyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNPcGVuLm9uKCAnY2xpY2snLCBhcHAudG9nZ2xlb2ZmQ2FudmFzICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc1NjcmVlbi5vbiggJ2NsaWNrJywgYXBwLmNsb3Nlb2ZmQ2FudmFzICk7XG5cdH07XG5cblx0Ly8gRG8gd2UgbWVldCB0aGUgcmVxdWlyZW1lbnRzP1xuXHRhcHAubWVldHNSZXF1aXJlbWVudHMgPSBmdW5jdGlvbigpIHtcblx0XHRyZXR1cm4gYXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5sZW5ndGg7XG5cdH07XG5cblx0Ly8gVG8gc2hvdyBvciBub3QgdG8gc2hvdz9cblx0YXBwLnRvZ2dsZW9mZkNhbnZhcyA9IGZ1bmN0aW9uKCkge1xuXG5cdFx0aWYgKCAndHJ1ZScgPT09ICQoIHRoaXMgKS5hdHRyKCAnYXJpYS1leHBhbmRlZCcgKSApIHtcblx0XHRcdGFwcC5jbG9zZW9mZkNhbnZhcygpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRhcHAub3Blbm9mZkNhbnZhcygpO1xuXHRcdH1cblxuXHR9O1xuXG5cdC8vIFNob3cgdGhhdCBkcmF3ZXIhXG5cdGFwcC5vcGVub2ZmQ2FudmFzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5hZGRDbGFzcyggJ2lzLXZpc2libGUnICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYWRkQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNTY3JlZW4uYWRkQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCB0cnVlICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5hdHRyKCAnYXJpYS1oaWRkZW4nLCBmYWxzZSApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5maW5kKCAnYnV0dG9uJyApLmZpcnN0KCkuZm9jdXMoKTtcblx0fTtcblxuXHQvLyBDbG9zZSB0aGF0IGRyYXdlciFcblx0YXBwLmNsb3Nlb2ZmQ2FudmFzID0gZnVuY3Rpb24oKSB7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc0NvbnRhaW5lci5yZW1vdmVDbGFzcyggJ2lzLXZpc2libGUnICk7XG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4ucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNTY3JlZW4ucmVtb3ZlQ2xhc3MoICdpcy12aXNpYmxlJyApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uYXR0ciggJ2FyaWEtZXhwYW5kZWQnLCBmYWxzZSApO1xuXHRcdGFwcC4kYy5vZmZDYW52YXNDb250YWluZXIuYXR0ciggJ2FyaWEtaGlkZGVuJywgdHJ1ZSApO1xuXG5cdFx0YXBwLiRjLm9mZkNhbnZhc09wZW4uZm9jdXMoKTtcblx0fTtcblxuXHQvLyBDbG9zZSBkcmF3ZXIgaWYgXCJlc2NcIiBrZXkgaXMgcHJlc3NlZC5cblx0YXBwLmVzY0tleUNsb3NlID0gZnVuY3Rpb24oIGV2ZW50ICkge1xuXHRcdGlmICggMjcgPT09IGV2ZW50LmtleUNvZGUgKSB7XG5cdFx0XHRhcHAuY2xvc2VvZmZDYW52YXMoKTtcblx0XHR9XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xuXG59KCB3aW5kb3csIGpRdWVyeSwgd2luZG93Lndkc29mZkNhbnZhcyApICk7XG4iLCIvKipcbiAqIEZpbGUgc2tpcC1saW5rLWZvY3VzLWZpeC5qcy5cbiAqXG4gKiBIZWxwcyB3aXRoIGFjY2Vzc2liaWxpdHkgZm9yIGtleWJvYXJkIG9ubHkgdXNlcnMuXG4gKlxuICogTGVhcm4gbW9yZTogaHR0cHM6Ly9naXQuaW8vdldkcjJcbiAqL1xuKCBmdW5jdGlvbigpIHtcblx0dmFyIGlzV2Via2l0ID0gLTEgPCBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ3dlYmtpdCcgKSxcblx0XHRpc09wZXJhID0gLTEgPCBuYXZpZ2F0b3IudXNlckFnZW50LnRvTG93ZXJDYXNlKCkuaW5kZXhPZiggJ29wZXJhJyApLFxuXHRcdGlzSWUgPSAtMSA8IG5hdmlnYXRvci51c2VyQWdlbnQudG9Mb3dlckNhc2UoKS5pbmRleE9mKCAnbXNpZScgKTtcblxuXHRpZiAoICggaXNXZWJraXQgfHwgaXNPcGVyYSB8fCBpc0llICkgJiYgZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQgJiYgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIgKSB7XG5cdFx0d2luZG93LmFkZEV2ZW50TGlzdGVuZXIoICdoYXNoY2hhbmdlJywgZnVuY3Rpb24oKSB7XG5cdFx0XHR2YXIgaWQgPSBsb2NhdGlvbi5oYXNoLnN1YnN0cmluZyggMSApLFxuXHRcdFx0XHRlbGVtZW50O1xuXG5cdFx0XHRpZiAoICEgKCAvXltBLXowLTlfLV0rJC8gKS50ZXN0KCBpZCApICkge1xuXHRcdFx0XHRyZXR1cm47XG5cdFx0XHR9XG5cblx0XHRcdGVsZW1lbnQgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCggaWQgKTtcblxuXHRcdFx0aWYgKCBlbGVtZW50ICkge1xuXHRcdFx0XHRpZiAoICEgKCAvXig/OmF8c2VsZWN0fGlucHV0fGJ1dHRvbnx0ZXh0YXJlYSkkL2kgKS50ZXN0KCBlbGVtZW50LnRhZ05hbWUgKSApIHtcblx0XHRcdFx0XHRlbGVtZW50LnRhYkluZGV4ID0gLTE7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRlbGVtZW50LmZvY3VzKCk7XG5cdFx0XHR9XG5cdFx0fSwgZmFsc2UgKTtcblx0fVxufSgpICk7XG4iLCIvKipcbiAqIEZpbGUgd2luZG93LXJlYWR5LmpzXG4gKlxuICogQWRkIGEgXCJyZWFkeVwiIGNsYXNzIHRvIDxib2R5PiB3aGVuIHdpbmRvdyBpcyByZWFkeS5cbiAqL1xud2luZG93Lndkc1dpbmRvd1JlYWR5ID0ge307XG4oIGZ1bmN0aW9uKCB3aW5kb3csICQsIGFwcCApIHtcblxuXHQvLyBDb25zdHJ1Y3Rvci5cblx0YXBwLmluaXQgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuY2FjaGUoKTtcblx0XHRhcHAuYmluZEV2ZW50cygpO1xuXHR9O1xuXG5cdC8vIENhY2hlIGRvY3VtZW50IGVsZW1lbnRzLlxuXHRhcHAuY2FjaGUgPSBmdW5jdGlvbigpIHtcblx0XHRhcHAuJGMgPSB7XG5cdFx0XHQnd2luZG93JzogJCggd2luZG93ICksXG5cdFx0XHQnYm9keSc6ICQoIGRvY3VtZW50LmJvZHkgKVxuXHRcdH07XG5cdH07XG5cblx0Ly8gQ29tYmluZSBhbGwgZXZlbnRzLlxuXHRhcHAuYmluZEV2ZW50cyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy53aW5kb3cubG9hZCggYXBwLmFkZEJvZHlDbGFzcyApO1xuXHR9O1xuXG5cdC8vIEFkZCBhIGNsYXNzIHRvIDxib2R5Pi5cblx0YXBwLmFkZEJvZHlDbGFzcyA9IGZ1bmN0aW9uKCkge1xuXHRcdGFwcC4kYy5ib2R5LmFkZENsYXNzKCAncmVhZHknICk7XG5cdH07XG5cblx0Ly8gRW5nYWdlIVxuXHQkKCBhcHAuaW5pdCApO1xufSggd2luZG93LCBqUXVlcnksIHdpbmRvdy53ZHNXaW5kb3dSZWFkeSApICk7XG4iXX0=
