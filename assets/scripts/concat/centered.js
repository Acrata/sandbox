/**
 * Center Menu logo
 *
 * @author Hector Ovalles
 */
Barba.Pjax.start();
window.CenteredMenu = {};
( function( window, $, app ) {
  var averageMenuItems = $("#centered-menu > li").size()/2-1;
  $(".site-header-centered .site-branding").insertAfter("#centered-menu li:nth("+ averageMenuItems +")");
  var lastElementClicked;
  Barba.Dispatcher.on('linkClicked', function(el) {
  lastElementClicked = el;
  console.log(el);
});
Barba.Dispatcher.on('newPageReady', function(currentStatus, oldStatus, container) {
console.log("new page ready");
});
  var FadeTransition = Barba.BaseTransition.extend({
    start: function() {
      /**
       * This function is automatically called as soon the Transition starts
       * this.newContainerLoading is a Promise for the loading of the new container
       * (Barba.js also comes with an handy Promise polyfill!)
       */

      // As soon the loading is finished and the old page is faded out, let's fade the new page
      Promise
        .all([this.newContainerLoading, this.fadeOut()])
        .then(this.fadeIn.bind(this));
          //  console.log($(".hero-image"));
    },

    fadeOut: function() {
      /**
       * this.oldContainer is the HTMLElement of the old Container
       */
      return $(this.oldContainer).addClass('is-changing-page').delay(1200).promise();
    },

    fadeIn: function() {
      /**
       * this.newContainer is the HTMLElement of the new Container
       * At this stage newContainer is on the DOM (inside our #barba-container and with visibility: hidden)
       * Please note, newContainer is available just after newContainerLoading is resolved!
       */

      var _this = this;
      var $el = $(this.newContainer);
      var heroImage = $el.context.childNodes[1].childNodes[1];
       console.log($el.context.childNodes[1].childNodes[1]);
       // console.log(Barba.Pjax.Dom.dataNamespace);
       var children = $el.context.childNodes[1];
//children.forEach(function(item){
    //console.log(item);
// });
      //console.log($("#page"));
      // $("#page").css("transform","translate(50%)");
      //$(this.oldContainer).hide();
      console.log(TweenLite);

    console.log(TweenLite.from(heroImage, 1.3, {
        top: "123px",
      }));
      $el.css({
        visibility : 'visible',
        opacity : 0
      });

      $el.animate({ opacity: 1  }, 400, function() {
        /**
         * Do not forget to call .done() as soon your transition is finished!
         * .done() will automatically remove from the DOM the old Container
         */

        _this.done();
      });
    }
  });

  /**
   * Next step, you have to tell Barba to use the new Transition
   */

  Barba.Pjax.getTransition = function() {
    /**
     * Here you can use your own logic!
     * For example you can use different Transition based on the current page or link...
     */

    return FadeTransition;
  };
  var Homepage = Barba.BaseView.extend({
  namespace: 'home',
  onEnter: function() {
      // The new Container is ready and attached to the DOM.
      console.log("enter home");
  },
  onEnterCompleted: function() {
      // The Transition has just finished.
  },
  onLeave: function() {
      // A new Transition toward a new page has just started.
  },
  onLeaveCompleted: function() {
      // The Container has just been removed from the DOM.
  }
});

// Don't forget to init the view!
Homepage.init();

} ( window, jQuery, window.CenteredMenu ) )
