window.FixedMenu = {};
( function( window, $, app) {
var headroom = new Headroom();
$("#header").headroom({
  "offset": 20,
  "tolerance": 5,
  "classes": {
    "initial": "animated",
    "pinned": "slideInUp",
    "unpinned": "slideInDown"
  }
});

} ( window, jQuery, window.FixedMenu ))
