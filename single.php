<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sandbox
 */

get_header(); ?>


	<div class="primary content-area col-l-8">
		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) :
			the_post();
			get_template_part( 'template-parts/content', get_post_format() );

			the_post_navigation();

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->
		<?php

		$menus = get_registered_nav_menus();

		foreach ( $menus as $location => $description ) {

			echo $location . ': ' . $description . '<br />';
		}
		 ?>
	</div><!-- .primary -->

	<?php get_sidebar(); ?>

<?php get_footer(); ?>
