<?php /* Markup for a single comment when inserted into the DOM */ ?>
<script type="text/html" id="tmpl-comment-single">

          <article class="post">

              <header class="entry-header">
                  <h1 class="entry-title">{{ data.post_title }}</h1>
                  <span class="entry-author">{{ data.post_author }}</span>
              </header>

              <div class="entry-content">
                  {{{ data.post_content }}}
              </div>

          </article>

      </script>
