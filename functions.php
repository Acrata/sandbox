<?php
/**
 * sandbox functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sandbox
 */

if ( ! function_exists( 'sandbox_dev_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sandbox_dev_setup() {
		/**
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on sandbox, use a find and replace
		 * to change 'sandbox-dev' to the name of your theme in all the template files.
		 * You will also need to update the Gulpfile with the new text domain
		 * and matching destination POT file.
		 */
		load_theme_textdomain( 'sandbox-dev', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/**
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/**
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		add_image_size( 'full-width', 1920, 1080, false );

		// Register navigation menus.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary Menu', 'sandbox-dev' ),
			'mobile'  => esc_html__( 'Mobile Menu', 'sandbox-dev' ),
			'wide'  => esc_html__( 'Wide Menu', 'sandbox-dev' ),
		) );

		/**
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'sandbox_dev_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Custom logo support.
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 500,
			'flex-height' => true,
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}
endif; // sandbox_dev_setup
add_action( 'after_setup_theme', 'sandbox_dev_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sandbox_dev_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sandbox_dev_content_width', 640 );
}
add_action( 'after_setup_theme', 'sandbox_dev_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function sandbox_dev_widgets_init() {

	// Define sidebars.
	$sidebars = array(
		'sidebar-1'  => esc_html__( 'Sidebar 1', 'sandbox-dev' ),
		 'sidebar-2'  => esc_html__( 'Sidebar 2', 'sandbox-dev' ),
		// 'sidebar-3'  => esc_html__( 'Sidebar 3', 'sandbox-dev' ),
	);

	// Loop through each sidebar and register.
	foreach ( $sidebars as $sidebar_id => $sidebar_name ) {
		register_sidebar( array(
			'name'          => $sidebar_name,
			'id'            => $sidebar_id,
			'description'   => /* translators: the sidebar name */ sprintf( esc_html__( 'Widget area for %s', 'sandbox-dev' ), $sidebar_name ),
			'before_widget' => '<aside class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}

}
add_action( 'widgets_init', 'sandbox_dev_widgets_init' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load styles and scripts.
 */
require get_template_directory() . '/inc/scripts.php';

/**
 * Load custom ACF features.
 */
require get_template_directory() . '/inc/acf.php';

/**
 * Load custom filters and hooks.
 */
require get_template_directory() . '/inc/hooks.php';

/**
 * Load custom queries.
 */
require get_template_directory() . '/inc/queries.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer/customizer.php';

/**
 * Scaffolding Library.
 */
require get_template_directory() . '/inc/scaffolding.php';

/**
 * Piklist Framework
 */
require get_template_directory() . '/inc/piklist.php';
/**
 * Scaffolding Library.
 */
// require get_template_directory() . '/inc/hmenu.php';
/**
 * //////////////////////////////////////////////////////////////////////
 */

/**
 * Testing hooks
 */
 	function add_single_classes($classes) {
 		// Adds a class of group-blog to blogs with more than 1 published author.
 		if ( is_single() ) {
 			$classes[] = 'sndb-blog';
 		}
 		return $classes;
 	}
 	add_filter('sandbox_dev_body_classes', 'add_single_classes');

/**
 * Add meta boxes
 */
add_action( 'add_meta_boxes', 'cd_meta_box_add' );
function cd_meta_box_add()
{
  add_meta_box( 'my-meta-box-id', 'Mi primer meta box', 'cd_meta_box_cb', 'post', 'normal', 'high' );
  add_meta_box( 'mymeta-box-id', 'Mi segundo meta box', 'rm_meta_box_callback',  array('page' ,'post' ), 'normal', 'high' );
}
//Add field
function rm_meta_box_callback( $meta_id ) {

    $outline = '<label for="title_field" style="width:150px; display:inline-block;">'. esc_html__('Title Field', 'text-domain') .'</label>';
    $title_field = get_post_meta( $meta_id->ID, 'title_field', true );
    $outline .= '<input type="text" name="title_field" id="title_field" class="title_field" value="'. esc_attr($title_field) .'" style="width:300px;"/>';

    echo $outline;
}
function cd_meta_box_cb()
{
    // $post is already set, and contains an object: the WordPress post
    global $post;
    $values = get_post_custom( $post->ID );
    $text = isset( $values['my_meta_box_text'] ) ? $values['my_meta_box_text'] : '';
    $selected = isset( $values['my_meta_box_select'] ) ? esc_attr( $values['my_meta_box_select'] ) : '';
    $check = isset( $values['my_meta_box_check'] ) ? esc_attr( $values['my_meta_box_check'] ) : '';

    // We'll use this nonce field later on when saving.
    wp_nonce_field( 'my_meta_box_nonce', 'meta_box_nonce' );
    ?>
    <p>
        <label for="my_meta_box_text">Text Label</label>
        <input type="text" name="my_meta_box_text" id="my_meta_box_text" value="<?php echo $text; ?>" />
    </p>

    <p>
        <label for="my_meta_box_select">Color</label>
        <select name="my_meta_box_select" id="my_meta_box_select">
            <option value="red" <?php selected( $selected, 'red' ); ?>>Red</option>
            <option value="blue" <?php selected( $selected, 'blue' ); ?>>Blue</option>
        </select>
    </p>

    <p>
        <input type="checkbox" id="my_meta_box_check" name="my_meta_box_check" <?php checked( $check, 'on' ); ?> />
        <label for="my_meta_box_check">Do not check this</label>
    </p>
    <?php
}

/**
 * Add theme
 */

 add_filter('piklist_admin_pages', 'piklist_theme_setting_pages');
   function piklist_theme_setting_pages($pages) {
      $pages[] = array(
       'page_title' => __('Tattoo theme Settings')
       ,'menu_title' => __('Theme Settings', 'piklist')
       ,'sub_menu' => 'themes.php' //Under Appearance menu
       ,'capability' => 'manage_options'
       ,'menu_slug' => 'custom_settings'
       ,'setting' => 'tattoo_theme_settings'
       ,'menu_icon' => plugins_url('piklist/parts/img/piklist-icon.png')
       ,'page_icon' => plugins_url('piklist/parts/img/piklist-page-icon-32.png')
       ,'single_line' => true
       ,'default_tab' => 'Basic'
       ,'save_text' => 'Save Demo Settings'
     );

     return $pages;
   }


	 function my_register_menu_metabox() {
	 	$custom_param = array( 0 => 'This param will be passed to my_render_menu_metabox' );

	 	add_meta_box( 'my-menu-test-metabox', 'Test Menu Metabox', 'my_render_menu_metabox', 'nav-menus', 'side', 'default', $custom_param );
	 }
	 add_action( 'admin_head-nav-menus.php', 'my_register_menu_metabox' );
	 /**
	  * Displays a menu metabox
	  *
	  * @param string $object Not used.
	  * @param array $args Parameters and arguments. If you passed custom params to add_meta_box(),
	  * they will be in $args['args']
	  */
	 function my_render_menu_metabox( $object, $args ) {
	 	global $nav_menu_selected_id;
	 	// Create an array of objects that imitate Post objects
	 	$my_items = array(
	 		(object) array(
	 			'ID' => 1,
	 			'db_id' => 0,
	 			'menu_item_parent' => 0,
	 			'object_id' => 1,
	 			'post_parent' => 0,
	 			'type' => 'my-custom-type',
	 			'object' => 'my-object-slug',
	 			'type_label' => 'My Cool Plugin',
	 			'title' => 'Custom Link 1',
	 			'url' => home_url( '/custom-link-1/' ),
	 			'target' => '',
	 			'attr_title' => '',
	 			'description' => '',
	 			'classes' => array(),
	 			'xfn' => '',
	 		),
	 		(object) array(
	 			'ID' => 2,
	 			'db_id' => 0,
	 			'menu_item_parent' => 0,
	 			'object_id' => 2,
	 			'post_parent' => 0,
	 			'type' => 'my-custom-type',
	 			'object' => 'my-object-slug',
	 			'type_label' => 'My Cool Plugin',
	 			'title' => 'Custom Link 2',
	 			'url' => home_url( '/custom-link-2/' ),
	 			'target' => '',
	 			'attr_title' => '',
	 			'description' => '',
	 			'classes' => array(),
	 			'xfn' => '',
	 		),
	 		(object) array(
	 			'ID' => 3,
	 			'db_id' => 0,
	 			'menu_item_parent' => 0,
	 			'object_id' => 3,
	 			'post_parent' => 0,
	 			'type' => 'my-custom-type',
	 			'object' => 'my-object-slug',
	 			'type_label' => 'My Cool Plugin',
	 			'title' => 'Custom Link 3',
	 			'url' => home_url( '/custom-link-3/' ),
	 			'target' => '',
	 			'attr_title' => '',
	 			'description' => '',
	 			'classes' => array(),
	 			'xfn' => '',
	 		),
	 	);
	 	$db_fields = false;
	 	// If your links will be hieararchical, adjust the $db_fields array bellow
	 	if ( false ) {
	 		$db_fields = array( 'parent' => 'parent', 'id' => 'post_parent' );
	 	}
	 	$walker = new Walker_Nav_Menu_Checklist( $db_fields );
	 	$removed_args = array(
	 		'action',
	 		'customlink-tab',
	 		'edit-menu-item',
	 		'menu-item',
	 		'page-tab',
	 		'_wpnonce',
	 	); ?>
	 	<div id="my-plugin-div">
	 		<div id="tabs-panel-my-plugin-all" class="tabs-panel tabs-panel-active">
	 		<ul id="my-plugin-checklist-pop" class="categorychecklist form-no-clear" >
	 			<?php echo walk_nav_menu_tree( array_map( 'wp_setup_nav_menu_item', $my_items ), 0, (object) array( 'walker' => $walker ) ); ?>
	 		</ul>

	 		<p class="button-controls">
	 			<span class="list-controls">
	 				<a href="<?php
	 					echo esc_url(add_query_arg(
	 						array(
	 							'my-plugin-all' => 'all',
	 							'selectall' => 1,
	 						),
	 						remove_query_arg( $removed_args )
	 					));
	 				?>#my-menu-test-metabox" class="select-all"><?php _e( 'Select All' ); ?></a>
	 			</span>

	 			<span class="add-to-menu">
	 				<input type="submit"<?php wp_nav_menu_disabled_check( $nav_menu_selected_id ); ?> class="button-secondary submit-add-to-menu right" value="<?php esc_attr_e( 'Add to Menu' ); ?>" name="add-my-plugin-menu-item" id="submit-my-plugin-div" />
	 				<span class="spinner"></span>
	 			</span>
	 		</p>
	 	</div>
	 	<?php
	 }

	 function op_register_menu_meta_box() {
	     add_meta_box(
	         'op-menu-meta-box-id',
	         esc_html__( 'Op Menu MetaBox Title', 'text-domain' ),
	         'op_render_menu_meta_box',
	         'nav-menus',
	         'side',
	         'core'
	         );
	 }
	 add_action( 'load-nav-menus.php', 'op_register_menu_meta_box' );

	 function op_render_menu_meta_box() {
	     // Metabox content
	     echo '<strong>Hi, I am MetaBox.</strong>';
	 }


	 function lw_comment_templates() {
	   include_once get_stylesheet_directory() . "/templates/comments-templates.php";
	 }
	 add_action( "wp_footer", "lw_comment_templates" );

	 add_action( 'wp_footer', 'jt_print_scripts', 25 );

function jt_print_scripts() { ?>

    <script type="text/javascript">
        jQuery( document ).ready( function() {

            var post_template = wp.template( 'comment-single' );
						// Load an existing post
var post = new wp.api.models.Post( { id: 15 } );
post.fetch();
  // console.log(post);
  // console.log(post.attributes);
	var datos = {};
	fetch('http://localhost:8888/sandbox/wp-json/wp/v2/posts/15')
	.then(function(response) {
	  return response.json();
	}).then(function(datosres) {
			datos = datosres;
			// console.log(datos);
			return datos;

	});
			 console.log(datos);

// Get a collection of the post's categories (returns a promise)
// Uses _embedded data if available, in which case promise resolves immediately.
post.getCategories().then( function( postCategories ) {
  // ... do something with the categories.
  // The new post has an single Category: Uncategorized
  // response -> "Uncategorized"
} );
            var data = {
                post_title   : 'This is awesome!',
                post_author  : 'Justin Tadlock',
                post_content : '<p>This is the content of an example post.</p>'
            }
							console.log(data);
            jQuery( '#page' ).append( post_template( data ) );
        } );
    </script>
<?php }


// add_action( 'wp_footer', function()
// {
//     if ( empty ( $GLOBALS['wp_widget_factory'] ) )
//         return;
//
//     $widgets = array_keys( $GLOBALS['wp_widget_factory']->widgets );
//     print '<pre>$widgets = ' . esc_html( var_export( $widgets, TRUE ) ) . '</pre>';
// });



//add_action( 'admin_init', 'obi_add_meta_box' );
// load-nav-menus.php is the perfect hook for this metabox
// as we are only adding on nav menu page :)
add_action( 'load-nav-menus.php', 'obi_add_meta_box' );
function obi_add_meta_box() {
	add_meta_box(
		'obi-nav-metabox',
		__( 'One Page Navigation', 'obi' ),
		'obi_render_nav_metabox',
		'nav-menus',
		'side',
		'core'
		);
}
function obi_render_nav_metabox() {
	echo '<strong>Hello World, This is Obi Plabon</strong>';
}
