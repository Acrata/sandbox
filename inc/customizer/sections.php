<?php
/**
 * Customizer sections.
 *
 * @package sandbox
 */

/**
 * Register the section sections.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function sandbox_dev_customize_sections( $wp_customize ) {

	// Register additional scripts section.
	$wp_customize->add_section(
		'sandbox_dev_additional_scripts_section',
		array(
			'title'    => esc_html__( 'Additional Scripts', 'sandbox-dev' ),
			'priority' => 10,
			'panel'    => 'site-options',
		)
	);

	// Register a social links section.
	$wp_customize->add_section(
		'sandbox_dev_social_links_section',
		array(
			'title'       => esc_html__( 'Social Media', 'sandbox-dev' ),
			'description' => esc_html__( 'Links here power the display_social_network_links() template tag.', 'sandbox-dev' ),
			'priority'    => 90,
			'panel'       => 'site-options',
		)
	);

	// Register a header section.
	$wp_customize->add_section(
		'sandbox_dev_header_section',
		array(
			'title'    => esc_html__( 'Header Customizations', 'sandbox-dev' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);

	// Register a footer section.
	$wp_customize->add_section(
		'sandbox_dev_footer_section',
		array(
			'title'    => esc_html__( 'Footer Customizations', 'sandbox-dev' ),
			'priority' => 90,
			'panel'    => 'site-options',
		)
	);
}
add_action( 'customize_register', 'sandbox_dev_customize_sections' );
