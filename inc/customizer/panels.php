<?php
/**
 * Customizer panels.
 *
 * @package sandbox
 */

/**
 * Add a custom panels to attach sections too.
 *
 * @param object $wp_customize Instance of WP_Customize_Class.
 */
function sandbox_dev_customize_panels( $wp_customize ) {

	// Register a new panel.
	$wp_customize->add_panel(
		'site-options', array(
			'priority'       => 10,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Site Options', 'sandbox-dev' ),
			'description'    => esc_html__( 'Other theme options.', 'sandbox-dev' ),
		)
	);

}
add_action( 'customize_register', 'sandbox_dev_customize_panels' );

function sandbox_dev_customize_panels_B( $wp_customize ) {

	// Register a new panel.
	$wp_customize->add_panel(
		'site-options', array(
			'priority'       => 9,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => esc_html__( 'Site Options B', 'sandbox-dev' ),
			'description'    => esc_html__( 'Other theme options. B', 'sandbox-dev' ),
		)
	);

}
add_action( 'customize_register', 'sandbox_dev_customize_panels_B' );
