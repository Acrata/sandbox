<?php
/**
 * Custom scripts and styles.
 *
 * @package sandbox
 */

/**
 * Register Google font.
 *
 * @link http://themeshaper.com/2014/08/13/how-to-add-google-fonts-to-wordpress-themes/
 */
function sandbox_dev_font_url() {

	$fonts_url = '';

	/**
	 * Translators: If there are characters in your language that are not
	 * supported by the following, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$roboto = esc_html_x( 'on', 'Roboto font: on or off', 'sandbox-dev' );
	$open_sans = esc_html_x( 'on', 'Open Sans font: on or off', 'sandbox-dev' );

	if ( 'off' !== $roboto || 'off' !== $open_sans ) {
		$font_families = array();

		if ( 'off' !== $roboto ) {
			$font_families[] = 'Roboto:400,700';
		}

		if ( 'off' !== $open_sans ) {
			$font_families[] = 'Open Sans:400,300,700';
		}

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
		);

		$fonts_url = add_query_arg( $query_args, '//fonts.googleapis.com/css' );
	}

	return $fonts_url;
}

/**
 * Enqueue scripts and styles.
 */
function sandbox_dev_scripts() {
	/**
	 * If WP is in script debug, or we pass ?script_debug in a URL - set debug to true.
	 */
	$debug = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ) || ( isset( $_GET['script_debug'] ) ) ? true : false;

	/**
	 * If we are debugging the site, use a unique version every page load so as to ensure no cache issues.
	 */
	$version = '1.0.0';

	/**
	 * Should we load minified files?
	 */
	$suffix = ( true === $debug ) ? '' : '.min';

	/**
	 * Global variable for IE.
	 */
	global $is_IE;

	// Register styles & scripts.
	wp_register_style( 'sandbox-dev-google-font', sandbox_dev_font_url(), array(), null );
	wp_register_style( 'slick-carousel', get_template_directory_uri() . '/assets/bower_components/slick-carousel/slick/slick.css', null, '1.6.0' );
	wp_register_script( 'slick-carousel', get_template_directory_uri() . '/assets/bower_components/slick-carousel/slick/slick' . $suffix . '.js', array( 'jquery' ), '1.6.0', true );
	wp_register_script( 'headroom', get_template_directory_uri() . '/assets/bower_components/headroom.js/dist/headroom.js' , array( 'jquery' ), '1.6.0', true );
	wp_register_script( 'headroomjQuery', get_template_directory_uri() . '/assets/bower_components/headroom.js/dist/jQuery.headroom.js' , array( 'jquery' ), '1.6.0', true );
	wp_register_script( 'lightbox_2', get_template_directory_uri() . '/assets/bower_components/lightbox2/dist/js/lightbox.js' , array( 'jquery' ), '1.6.0', true );
	wp_register_script( 'macy', get_template_directory_uri() . '/assets/bower_components/macy/dist/macy.js' , array( 'jquery' ), '1.6.0', true );
	wp_register_script( 'masonry-sandbox', get_template_directory_uri() . '/assets/bower_components/masonry-layout/dist/masonry.pkgd.js' , array( 'jquery' ), '1.6.0', true );
	wp_register_script( 'gsap', get_template_directory_uri() . '/assets/bower_components/gsap/src/uncompressed/TweenMax.js' , array( 'jquery' ), '1.20.4', true );

	// Enqueue styles.
	wp_enqueue_style( 'sandbox-dev-google-font' );
	wp_enqueue_style( 'sandbox-dev-style', get_stylesheet_directory_uri() . '/style' . $suffix . '.css', array(), $version );
	wp_enqueue_style( 'lightbox2-css', get_stylesheet_directory_uri() . '/assets/bower_components/lightbox2/dist/css/lightbox.min.css', array(), '1.0.0' );
wp_enqueue_script( 'headroom' );
wp_enqueue_script( 'headroomjQuery' );
wp_enqueue_script( 'lightbox_2' );
wp_enqueue_script( 'macy' );
wp_enqueue_script( 'gsap' );
   // wp_enqueue_script('masonry');
wp_enqueue_script('masonry-sandbox');
wp_enqueue_script( 'wp-api' );
wp_enqueue_script( 'wp-utils' );

	// Enqueue scripts.
	if ( $is_IE ) {
		wp_enqueue_script( 'sandbox-dev-babel-polyfill', get_template_directory_uri() . '/assets/scripts/babel-polyfill.min.js', array(), $version, true );
	}

	wp_enqueue_script( 'sandbox-dev-scripts', get_template_directory_uri() . '/assets/scripts/project' . $suffix . '.js', array( 'jquery','wp-util','underscore' ), $version, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Enqueue the scaffolding Library script.
	if ( is_page_template( 'template-scaffolding.php' ) ) {
		wp_enqueue_script( 'sandbox-dev-scaffolding', get_template_directory_uri() . '/assets/scripts/scaffolding' . $suffix . '.js', array( 'jquery' ), $version, true );
	}
	wp_localize_script('ajax_test', 'wp_ajax_tets_vars', array(
    'ajaxurl' => admin_url( 'admin-ajax.php' )
));
}
add_action( 'wp_enqueue_scripts', 'sandbox_dev_scripts' );

/**
 * Enqueue scripts for the customizer.
 *
 * @author Corey Collins
 */
function sandbox_dev_customizer_scripts() {

	/**
	 * If WP is in script debug, or we pass ?script_debug in a URL - set debug to true.
	 */
	$debug = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG == true ) || ( isset( $_GET['script_debug'] ) ) ? true : false;

	/**
	 * If we are debugging the site, use a unique version every page load so as to ensure no cache issues.
	 */
	$version = '1.0.0';

	/**
	 * Should we load minified files?
	 */
	$suffix = ( true === $debug ) ? '' : '.min';

	wp_enqueue_script( 'sandbox_dev_customizer', get_template_directory_uri() . '/assets/scripts/customizer' . $suffix . '.js', array( 'jquery' ), $version, true );
}
add_action( 'customize_controls_enqueue_scripts', 'sandbox_dev_customizer_scripts' );
/**
 * Add SVG definitions to footer.
 */
function sandbox_dev_include_svg_icons() {

	// Define SVG sprite file.
	$svg_icons = get_template_directory() . '/assets/images/svg-icons.svg';

	// If it exists, include it.
	if ( file_exists( $svg_icons ) ) {
		require_once( $svg_icons );
	}
}
add_action( 'wp_footer', 'sandbox_dev_include_svg_icons', 9999 );
