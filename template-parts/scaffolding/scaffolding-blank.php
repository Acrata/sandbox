<?php
/**
 * The template used for displaying X in the scaffolding library.
 *
 * @package sandbox
 */

?>

<section class="section-scaffolding">
	<h2 class="scaffolding-heading"><?php esc_html_e( 'X', 'sandbox-dev' ); ?></h2>
</section>
