<?php
/**
 * Template Name: Full width artist`s profiles
 *
 * Template Post Type: artist
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sandbox
 */

get_header(); ?>
			<section class="hero-artist">
				<div class="text-content">
					<div class="text-centered">
						<h2>About Jade</h2>
						<p>Even though using “lorem ipsum” often arouses curiosity due to its resemblance to classical Latin, it is not intended to have meaning.</p><p>Where text is visible in a document, people tend to focus on the textual content rather than upon overall presentation, so publishers use lorem ipsum when displaying a typeface or design in order to direct the focus to presentation. “Lorem ipsum” also approximates a typical distribution of spaces in English.</p>
					</div>
				</div>
				<div class="img-content">

					<img src="https://demo.wolfthemes.com/tattoopro/wp-content/uploads/sites/13/2015/05/4998295763_546b93f07c_o-1024x680.jpg" alt="" class="">
				</div>
			</section>
			<section class="artist-gallery">
				<div class="gallery-artist-siha">
					<?php //echo do_shortco<?php
					 $gallery_artist_siha =  get_post_meta( $post->ID, 'upload_gallery_field', false);
					 echo do_shortcode( wpautop( get_post_meta( $post->ID, 'post_content_artist', true) ) );
					 // print_r($gallery_artist_siha);
						// foreach ($gallery_artist_siha as $item_gallery) {
						// 	echo "<div class='gallery-item-sandbox'>";
						// 	 echo wp_get_attachment_image($item_gallery, 'full');
						// 	echo "</div>";
						// 	// echo $item_gallery;
						// }
					 ?>
				</div>
			</section>

<?php get_footer(); ?>
