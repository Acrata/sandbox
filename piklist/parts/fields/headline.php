<?php
piklist('field', array(
   'type' => 'group',
   'field' => 'headline_group',
   'label' => 'Flexible Panels',
   'template' => 'field',

   'fields' => array(

      array(
         'type' => 'text',
         'field' => 'headline',
         'label' => 'Headline',
         'columns' => 10,
      ),

      array(
         'type' => 'number',
         'field' => 'headline_font_size',
         'label' => 'Font size',
         'columns' => 4,
      ),

      // Headline field is in all panels
      array(
         'type' => 'color',
         'field' => 'color_bg',
         'label' => 'Background color',
         'columns' => 2,
      ),
      array(
         'type' => 'select',
         'field' => 'choose_panel',
         'label' => 'Choose panel type',
         'columns' => 4,
         'choices' => array(
            'grid_3' => 'Three-column grid',
            'columns_2' => 'Two columns',
            'boxed_rows' => 'Boxed rows',
            'wide_content' => 'Wide content',
            'looking_forward' => 'Looking forward',
            'fifty_fifty' => 'Fifty/Fifty',
         ),
         'value' => 'grid_3',
      ),

    )
));
