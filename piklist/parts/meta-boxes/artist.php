<?php
/*
Title: Artist settings
Post Type: artist
*/
piklist('field', array(
  'type' => 'file'
  ,'field' => 'upload_gallery_field'
  ,'scope' => 'post_meta'
  ,'label' => 'Gallery Uploader'
  ,'options' => array(
    'modal_title' => 'Add File(s)'
    ,'button' => 'Add Gallery'
    
  )
));

piklist('field', array(
    'type' => 'editor',
    'field' => 'post_content_artist', // This is the field name of the WordPress default editor
    'scope' => 'post_meta', // Save to the wp_post table
    'label' => 'Post Content',
    'template' => 'field', // Only display the field not the label
    'options' => array( // Pass any option that is accepted by wp_editor()
      'wpautop' => true,
      'media_buttons' => true,
      'shortcode_buttons' => true,
      'teeny' => false,
      'dfw' => false,
      'quicktags' => true,
      'drag_drop_upload' => true,
      'tinymce' => array(
        'resize' => false,
        'wp_autoresize_on' => true
      )
    )
 ));

?>
