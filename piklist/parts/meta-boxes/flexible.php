<?php
/*
Title: Flexible content
Post Type: artist
*/
piklist('field', array(
 'type' => 'radio'
 ,'field' => 'subscribe_to_newsletter'
 ,'label' => 'Would you like to subscribe to our newsletter?'
 ,'attributes' => array(
 'class' => 'text'
 )
 ,'choices' => array(
   'yes' => 'Yes'
   ,'no' => 'No'
 )
 ,'value' => 'no'
));

piklist('field', array(
 'type' => 'text'
 ,'field' => 'email_address'
 ,'label' => 'Email Address'
 ,'description' => 'Please enter your email address'
 ,'conditions' => array(
   array(
    'field' => 'subscribe_to_newsletter'
    ,'value' => 'yes'
   )
 )
));

piklist('field', array(
   'type' => 'group',
   'field' => 'flex_panels',
   'add_more' => true,
   'label' => 'Flexible Panels 22',
   'template' => 'field',

   'fields' => array(

      // first select the type of panel; this will determine which fields to add
      array(
         'type' => 'select',
         'field' => 'choose_panels',
         'label' => 'Choose panel type',
         'choices' => array(
            'looking_forward' => 'Looking forward',
            'columns_2' => 'Fifty/Fifty',
         ),
         'value' => 'columns_2',
      ),

      // Headline field is in all panels
      array(
         'type' => 'headline',
         'field' => 'headline_type',
         'label' => 'Headline',
         'columns' => 10,
         'conditions' => array(
   array(
    'field' => 'flex_panels:choose_panels'
    ,'value' => 'looking_forward'
   )
 )
      ),

      // Headline field is in all panels
      array(
         'type' => 'color',
         'field' => 'color_bg',
         'label' => 'Background color',
         'columns' => 2,
      ),

    )
));

piklist('field', array(
  'type' => 'headline',
  'field' => 'headline_field',
  'label' => 'Headline'
));
