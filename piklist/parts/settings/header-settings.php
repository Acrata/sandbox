<?php
/*
Flow: Theme Workflow Sihaa
Tab: Header
Title: Color / Date Pickers
Order: 40
Setting: tattoo_theme_settings
*/


  piklist('field', array(
    'type' => 'select'
    ,'field' => 'select_menu_layout'
    ,'label' => __('Select header layout', 'piklist-demo')
    ,'value' => 'third'
    ,'choices' => array(
      'center' => __('Centered Menu', 'piklist-demo')
      ,'wide' => __('Wide Menu', 'piklist-demo')
    )
  ));
