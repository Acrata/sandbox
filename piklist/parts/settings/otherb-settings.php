<?php
/*
Flow: Theme Workflow Sihaa
Tab: Common
Title: Radio Fields b
Order: 22
Setting: tattoo_theme_settings
*/
  piklist('field', array(
    'type' => 'radio'
    ,'field' => 'radioad'
    ,'label' => __('Radio', 'piklist-demo')
    ,'value' => 'third'
    ,'choices' => array(
      'first' => __('First Choice', 'piklist-demo')
      ,'second' => __('Second Choice', 'piklist-demo')
      ,'third' => __('Third Choice', 'piklist-demo')
    )
  ));
